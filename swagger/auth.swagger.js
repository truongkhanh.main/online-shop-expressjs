/**
 * @swagger
 * components:
 *  securitySchemes:
 *    bearerAuth:
 *      type: http
 *      scheme: bearer
 *      bearerFormat: JWT
 *  security:
 *    - bearerAuth: []
 *  responses:
 *      '200':
 *        description: 'OK'
 *      '403': 
 *        description: 'Fail'
 */

/**
 * @swagger
 * components:
 *  responses:
 *    UnauthorizedError:
 *      description: Access token is missing or invalid
 */

/**
 * @swagger
 * /api/v1/auth/register:
 *   post:
 *     summary: Create a new user
 *     tags: [User]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *              type: object
 *              example:
 *                  id: 24a69dff-3c3b-4a92-870b-f3f43250264f
 *                  username: khanhtq1234
 *                  password: khanhtq1234
 *                  age: 22
 *                  email: khanhtq@gmail.com
 *                  phone: 0379416224
 *                  address: ha noi
 *                  isActive: true
 *     responses:
 *       200:
 *         description: The user was successfully created
 *         content:
 *           application/json:
 *             schema:
 *                type: object
 *                $ref: '#/components/schemas/User'
 *       500:
 *         description: Some server error
 */


/**
 * @swagger
 * /api/v1/auth/login:
 *   post:
 *     summary: Login into the system
 *     tags: [User]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *              type: object
 *              example:
 *                  username: admin
 *                  password: admin
 *     responses:
 *       200:
 *         description: Login successful
 *       500:
 *         description: Some server error
 */