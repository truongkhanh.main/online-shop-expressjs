/**
 * @swagger
 * tags:
 *   name: Flash Sale
 *   description: The flash sale managing API
 */

/**
 * @swagger
 * components:
 *  schemas:
 *      FlashSale:
 *          type: object
 *          required:
 *              - id
 *              - name
 *              - description
 *              - discountPercent
 *          properties:
 *              name:
 *                 type: string
 *                 description: The name of flash sale
 *              description:
 *                 type: string
 *                 description: The description of flash sale
 *              discountPercent:
 *                 type: integer
 *                 description: The amount sale of product
 *              expireTime:
 *                 type: integer
 *                 description: The time flash sale active
 */

/**
 * @swagger
 * /api/v1/flash-sales:
 *   post:
 *     summary: Create a flash sale
 *     tags: [Flash Sale]
 *     security:
 *        - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/FlashSale'
 *     responses:
 *       201:
 *         description: Create flash sale successful
 *       500:
 *         description: Server error
 */

/**
 * @swagger
 * /api/v1/flash-sales:
 *   get:
 *     summary: Returns the list of all the flash sale
 *     tags: [Flash Sale]
 *     security:
 *        - bearerAuth: []
 *     responses:
 *       200:
 *         description: The list of the flash sale
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/FlashSale'
 */

/**
 * @swagger
 * /api/v1/flash-sales/no-expire:
 *   get:
 *     summary: Returns the list of all the flash sale that not expire
 *     tags: [Flash Sale]
 *     security:
 *        - bearerAuth: []
 *     responses:
 *       200:
 *         description: The list of the flash sale have no expire
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 */

/**
 * @swagger
 * /api/v1/flash-sales/{id}:
 *   get:
 *     summary: Get the detail of a flash sale by id
 *     tags: [Flash Sale]
 *     security:
 *        - bearerAuth: []
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: The flash sale id
 *     responses:
 *       200:
 *         description: The detail of a flash sale
 */

/**
 * @swagger
 * /api/v1/flash-sales/{id}:
 *   delete:
 *     summary: Delete a flash sale by Id
 *     tags: [Flash Sale]
 *     security:
 *        - bearerAuth: []
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: The flash sale id
 *     responses:
 *       200:
 *         description: Delete flash sale successful
 */

/**
 * @swagger
 * /api/v1/flash-sales/{id}:
 *   patch:
 *     summary: Update the flash sale by Id
 *     tags: [Flash Sale]
 *     security:
 *        - bearerAuth: []
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: The flash sale id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/FlashSale'
 *     responses:
 *       200:
 *         description: Update flash sale successful
 *       404:
 *         description: The flash sale was not found
 *       500:
 *         description: Some error happened
 */