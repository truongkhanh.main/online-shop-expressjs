/**
 * @swagger
 * tags:
 *   name: Voucher
 *   description: The voucher managing API
 */

/**
 * @swagger
 * components:
 *  schemas:
 *      Voucher:
 *          type: object
 *          required:
 *              - name
 *              - description
 *              - code
 *              - discountPercent
 *              - quantity
 *          properties:
 *              name:
 *                 type: string
 *                 description: The name of voucher
 *              description:
 *                 type: string
 *                 description: The description of voucher
 *              code:
 *                 type: string
 *                 description: The code of voucher
 *              discountPercent:
 *                 type: integer
 *                 description: The amount sale of voucher
 *              expireTime:
 *                 type: integer
 *                 description: The time voucher active
 *              quantity:
 *                 type: integer
 *                 description: The quantity of voucher
 */

/**
 * @swagger
 * /api/v1/vouchers:
 *   post:
 *     summary: Create a voucher
 *     tags: [Voucher]
 *     security:
 *        - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Voucher'
 *     responses:
 *       201:
 *         description: Create voucher successful
 *       500:
 *         description: Server error
 */

/**
 * @swagger
 * /api/v1/vouchers:
 *   get:
 *     summary: Returns the list of all the voucher
 *     tags: [Voucher]
 *     security:
 *        - bearerAuth: []
 *     responses:
 *       200:
 *         description: The list of the vouchers
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Voucher'
 */

/**
 * @swagger
 * /api/v1/vouchers/no-expire:
 *   get:
 *     summary: Returns the list of all the voucher that not expire
 *     tags: [Voucher]
 *     security:
 *        - bearerAuth: []
 *     responses:
 *       200:
 *         description: The list of the vouchers have no expire
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 */

/**
 * @swagger
 * /api/v1/vouchers/{id}:
 *   get:
 *     summary: Get the detail of a voucher by id
 *     tags: [Voucher]
 *     security:
 *        - bearerAuth: []
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: The voucher id
 *     responses:
 *       200:
 *         description: The detail of a vouchers
 */

/**
 * @swagger
 * /api/v1/vouchers/{id}:
 *   delete:
 *     summary: Delete a voucher by Id
 *     tags: [Voucher]
 *     security:
 *        - bearerAuth: []
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: The voucher id
 *     responses:
 *       200:
 *         description: Delete voucher successful
 */

/**
 * @swagger
 * /api/v1/vouchers/{id}:
 *   patch:
 *     summary: Update the voucher by Id
 *     tags: [Voucher]
 *     security:
 *        - bearerAuth: []
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: The voucher id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Voucher'
 *     responses:
 *       200:
 *         description: Update voucher successful
 *       404:
 *         description: The voucher was not found
 *       500:
 *         description: Some error happened
 */