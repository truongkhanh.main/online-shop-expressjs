const swaggerUI = require('swagger-ui-express')
const swaggerJsDoc = require('swagger-jsdoc')
const path = require('path')

function swagger(app) {
    const option = {
        definition: {
            openapi: "3.0.0",
            info: {
                title: "Libraty API",
                version: "1.0.0",
                description: "A simple express Library API"
            },
            servers: [
                {
                    url: "http://localhost:3000"
                }
            ]
        },
        apis: [path.join(__dirname, "/*.js")]
    }

    const specs = swaggerJsDoc(option)

    app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(specs))
}

module.exports = swagger
