/**
 * @swagger
 * tags:
 *   name: Category
 *   description: The categories managing API
 */

/**
 * @swagger
 * components:
 *  schemas:
 *      Category:
 *          type: object
 *          required:
 *              - name
 *              - image
 *          properties:
 *              name:
 *                 type: string
 *                 description: The name of category
 *              image:
 *                 type: string
 *                 format: binary
 *                 description: The file name image of category
 */

/**
 * @swagger
 * components:
 *  schemas:
 *      SubCategory:
 *          type: object
 *          required:
 *              - name
 *              - image
 *              - categoryId
 *          properties:
 *              name:
 *                 type: string
 *                 description: The name of sub category
 *              image:
 *                 type: string
 *                 format: binary
 *                 description: The file name image of sub category
 *              categoryId:
 *                 type: string
 *                 description: The id of category
 */

/**
 * @swagger
 * /api/v1/categories/subcate:
 *   post:
 *     summary: Create a sub category
 *     tags: [Category]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/SubCategory'
 *     responses:
 *       201:
 *         description: The user was successfully created
 *         content:
 *           application/json:
 *             schema:
 *                type: object
 *                $ref: '#/components/schemas/SubCategory'
 *       500:
 *         description: Some server error
 */

/**
 * @swagger
 * /api/v1/categories/cate:
 *   post:
 *     summary: Create a category
 *     tags: [Category]
 *     security:
 *        - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/Category'
 *     responses:
 *       201:
 *         description: The user was successfully created
 *         content:
 *           application/json:
 *             schema:
 *                type: object
 *                $ref: '#/components/schemas/Category'
 *       500:
 *         description: Some server error
 */

/**
 * @swagger
 * /api/v1/categories/subcate/{id}:
 *   patch:
 *     summary: Update the sub category by Id
 *     tags: [Category]
 *     security:
 *        - bearerAuth: []
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: The sub category id
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/SubCategory'
 *     responses:
 *       200:
 *         description: Update sub category successful
 *       404:
 *         description: The sub category was not found
 *       500:
 *         description: Some error happened
 */

/**
 * @swagger
 * /api/v1/categories/cate/{id}:
 *   patch:
 *     summary: Update the category by Id
 *     tags: [Category]
 *     security:
 *        - bearerAuth: []
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: The category id
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/Category'
 *     responses:
 *       200:
 *         description: Update category successful
 *       404:
 *         description: The category was not found
 *       500:
 *         description: Some error happened
 */

/**
 * @swagger
 * /api/v1/categories/subcate/{id}:
 *   delete:
 *     summary: Delete a sub category by Id
 *     tags: [Category]
 *     security:
 *        - bearerAuth: []
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: The sub category id
 *     responses:
 *       200:
 *         description: Delete sub category successful
 *       500:
 *         description: Server error
 */

/**
 * @swagger
 * /api/v1/categories/cate/{id}:
 *   delete:
 *     summary: Delete a category by Id
 *     tags: [Category]
 *     security:
 *        - bearerAuth: []
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: The category id
 *     responses:
 *       200:
 *         description: Delete category successful
 *       500:
 *         description: Server error
 */

/**
 * @swagger
 * /api/v1/categories:
 *   get:
 *     summary: Returns the list of all the category and sub category
 *     tags: [Category]
 *     security:
 *        - bearerAuth: []
 *     responses:
 *       200:
 *         description: The list of the category and sub category
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Category'
 */

/**
 * @swagger
 * /api/v1/categories/{id}:
 *   get:
 *     summary: Get the detail of a category by id
 *     tags: [Category]
 *     security:
 *        - bearerAuth: []
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: The category id
 *     responses:
 *       200:
 *         description: The detail of a category
 */