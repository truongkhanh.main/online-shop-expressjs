/**
 * @swagger
 * tags:
 *   name: Order
 *   description: The order managing API
 */

/**
 * @swagger
 * components:
 *  schemas:
 *      Order:
 *          type: object
 *          required:
 *              - id
 *              - orderCode
 *              - status
 *              - totalPrice
 *              - isDeleted
 *              - createdAt
 *              - updatedAt
 *              - customerId
 *              - productId
 *          properties:
 *              id:
 *                  type: string
 *                  description: The id of the order
 *              orderCode:
 *                  type: string
 *                  description: The order code of order
 *              status:
 *                  type: integer
 *                  description: The status of the order
 *              totalPrice:
 *                  type: double
 *                  description: The total price of the order
 *              isDeleted:
 *                 type: boolean
 *                 description: The status of the order
 *              createdBy:
 *                  type: string
 *                  description: The id of user created order
 *              updatedBy:
 *                  type: string
 *                  description: The id of user updated order
 *              createdAt:
 *                  type: date
 *                  description: Time the order created
 *              updatedAt:
 *                  type: date
 *                  description: Time the order updated
 *              customerId:
 *                  type: string
 *                  description: The id of customer who created order
 *              productId:
 *                  type: array
 *                  description: The ids of products
 */

/**
 * @swagger
 * components:
 *  schemas:
 *      OrderDetail:
 *          type: object
 *          required:
 *              - id
 *              - quantity
 *              - price
 *              - isDeleted
 *              - createdAt
 *              - updatedAt
 *              - orderId
 *              - productId
 *          properties:
 *              id:
 *                  type: string
 *                  description: The id of the order detail
 *              quantity:
 *                  type: integer
 *                  description: The order code of product in order detail
 *              price:
 *                  type: integer
 *                  description: The price of product in order detail
 *              isDeleted:
 *                 type: boolean
 *                 description: The status of the order detail
 *              createdBy:
 *                  type: string
 *                  description: The id of user created order detail
 *              updatedBy:
 *                  type: string
 *                  description: The id of user updated order detail
 *              createdAt:
 *                  type: date
 *                  description: Time the order detail created
 *              updatedAt:
 *                  type: date
 *                  description: Time the order detail updated
 *              orderId:
 *                  type: string
 *                  description: The id of order
 *              productId:
 *                  type: string
 *                  description: The id of product
 */


/**
 * @swagger
 * /api/v1/orders:
 *  post:
 *      summary: create a new order
 *      description: post a new order
 *      tags: [Order]
 *      security: 
 *          - bearerAuth: []
 *      requestBody: 
 *          content: 
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Order'
 *      responses:
 *          '201':
 *              description: create success
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *          400:
 *              description: bad request
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 */

/**
 * @swagger
 * /api/v1/orders:
 *  get:
 *      summary: return list of the order
 *      description: get all order 
 *      tags: [Order]
 *      security: 
 *        - bearerAuth: []
 *      parameters:
 *        - in: query
 *          name: page
 *        - in: query
 *          name: size
 *      responses:
 *          200: 
 *              description: ok
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *          400:
 *              description: bad request
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 */

/**
 * @swagger
 * /api/v1/orders/{id}:
 *  get:
 *      summary: return a order
 *      description: get a order by id
 *      tags: [Order]
 *      security: 
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *              type: string
 *          required: true       
 *      responses:
 *          200: 
 *              description: ok
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *          400:
 *              description: bad request
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 */

/**
 * @swagger
 * /api/v1/orders/{id}:
 *  delete:
 *      summary: Delete a order
 *      description: Soft delete a order by id
 *      tags: [Order]
 *      security: 
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *              type: string
 *          required: true       
 *      responses:
 *          200: 
 *              description: ok
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *          400:
 *              description: bad request
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 */

/**
 * @swagger
 * /api/v1/orders/{id}/confirm:
 *  patch:
 *      summary: update status order 
 *      description: update status order to comfirm
 *      tags: [Order]
 *      security: 
 *        - bearerAuth: []
 *      parameters: 
 *        - in: path
 *          name: id 
 *          schema:
 *              type: string
 *          required: true
 *      responses:
 *          200: 
 *              description: ok
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *          400:
 *              description: bad request
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 */

/**
 * @swagger
 * /api/v1/orders/{id}/shipping:
 *  patch:
 *      summary: update status order 
 *      description: update status order to shipping 
 *      tags: [Order]
 *      security: 
 *        - bearerAuth: []
 *      parameters: 
 *        - in: path
 *          name: id 
 *          schema:
 *              type: string
 *          required: true
 *      responses:
 *          200: 
 *              description: ok
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *          400:
 *              description: bad request
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 */

/**
 * @swagger
 * /api/v1/orders/{id}/delivered:
 *  patch:
 *      summary: update status order
 *      description: update status order to delivered  
 *      tags: [Order]
 *      security: 
 *        - bearerAuth: []
 *      parameters: 
 *        - in: path
 *          name: id 
 *          schema:
 *              type: string
 *          required: true
 *      responses:
 *          200: 
 *              description: ok
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *          400:
 *              description: bad request
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 */

/**
 * @swagger
 * /api/v1/orders/{id}/cancel:
 *  patch:
 *      summary: update status order 
 *      description: update status order to cancel 
 *      tags: [Order]
 *      security: 
 *        - bearerAuth: []
 *      parameters: 
 *        - in: path
 *          name: id 
 *          schema:
 *              type: string
 *          required: true
 *      responses:
 *          200: 
 *              description: ok
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *          400:
 *              description: bad request
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 */