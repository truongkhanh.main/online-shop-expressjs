/**
 * @swagger
 * tags:
 *   name: User
 *   description: The users managing API
 */

/**
 * @swagger
 * components:
 *  schemas:
 *      User:
 *          type: object
 *          required:
 *              - username
 *              - password
 *              - isActive
 *          properties:
 *              id:
 *                 type: string
 *                 description: The id of the user
 *              username:
 *                 type: string
 *                 description: The username of user
 *              password:
 *                 type: string
 *                 description: The password of user
 *              age:
 *                 type: integer
 *                 description: The age of user
 *              email:
 *                 type: string
 *                 description: The email of user
 *              phone:
 *                 type: string
 *                 description: The phone number of user
 *              address:
 *                 type: string
 *                 description: The address of user
 *              isActive:
 *                 type: boolean
 *                 description: The user active or inactive
 *          example:
 *              id: 24a69dff-3c3b-4a92-870b-f3f43250264f
 *              username: khanhtq1234
 *              password: khanhtq1234
 *              age: 22
 *              email: khanhtq@gmail.com
 *              phone: 0379416224
 *              address: ha noi
 *              isActive: true
 */

/**
 * @swagger
 * components:
 *  schemas:
 *      Customer:
 *          type: object
 *          required:
 *              - userId
 *              - isActive
 *          properties:
 *              id:
 *                 type: string
 *                 description: The id of the user
 *              userId:
 *                 type: string
 *                 reference: User
 *                 description: The id of user
 *              paymentMethod:
 *                 type: integer
 *                 description: The payment method of customer
 *              isActive:
 *                 type: boolean
 *                 description: The customer active or inactive
 *          example:
 *              id: t5fdddff-3ddb-34rw-870b-f3f43dfrtyh5
 *              userId: 24a69dff-3c3b-4a92-870b-f3f43250264f
 *              paymentMethod: 123345
 *              isActive: true
 */

/**
 * @swagger
 * /api/v1/users:
 *   get:
 *     summary: Returns the list of all the users
 *     tags: [User]
 *     security:
 *        - bearerAuth: []
 *     responses:
 *       200:
 *         description: The list of the users
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 */

/**
 * @swagger
 * /api/v1/users/{id}:
 *   get:
 *     summary: Returns the user by Id
 *     tags: [User]
 *     security:
 *        - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The user id
 *     content:
 *       application/json:
 *         schema:
 *           $ref: '#/components/schemas/User'
 *     responses:
 *       200:
 *         description: The information of user
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/User'
 */


/**
 * @swagger
 * /api/v1/users/{id}:
 *  patch:
 *    summary: Update the user information by the id
 *    tags: [User]
 *    security:
 *       - bearerAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The user id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/User'
 *    responses:
 *      200:
 *        description: The user was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/User'
 *      404:
 *        description: The user was not found
 *      500:
 *        description: Some error happened
 */

/**
 * @swagger
 * /api/v1/users/{id}:
 *   delete:
 *     summary: Delete the user by Id
 *     tags: [User]
 *     security:
 *        - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The user id
 *     content:
 *       application/json:
 *         schema:
 *           $ref: '#/components/schemas/User'
 *     responses:
 *       200:
 *         description: Delete user
 */