/**
 * @swagger
 * tags:
 *   name: Product
 *   description: The products managing API
 */

/**
 * @swagger
 * components:
 *  schemas:
 *      Product:
 *          type: object
 *          required:
 *              - name
 *              - description
 *              - barcode
 *              - importPrice
 *              - price
 *              - quantity
 *              - subcategoryId
 *              - url
 *          properties:
 *              name:
 *                  type: string
 *                  description: The name of product
 *              description:
 *                  type: string
 *                  description: The description of the product
 *              barcode:
 *                  type: string
 *                  description: The bar code of the product
 *              importPrice:
 *                  type: double
 *                  description: The import price of the product
 *              price:
 *                  type: double
 *                  description: The price of the product
 *              quantity:
 *                  type: integer
 *                  description: The quantity on system of the product
 *              subcategoryId:
 *                  type: string
 *                  description: The id of subcategory
 *              url:
 *                  type: string
 *                  format: binary
 *                  description: The id of subcategory
 */

/**
 * @swagger
 * /api/v1/products:
 *   post:
 *     summary: Create a product
 *     tags: [Product]
 *     security:
 *        - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/Product'
 *     responses:
 *       200: 
 *          description: Response success
 *          content: 
 *              application/json:
 *                  schema:
 *                      type: object
 *       500:
 *         description: Server error
 *         content: 
 *             application/json:
 *                 schema:
 *                     type: object
 */

/**
 * @swagger
 * /api/v1/products:
 *  get:
 *      summary: Return list of the product
 *      description: Get all product 
 *      tags: [Product]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: query
 *          name: page
 *        - in: query
 *          name: size
 *      responses:
 *          200: 
 *             description: Response success
 *             content: 
 *                 application/json:
 *                     schema:
 *                         type: object
 *          400:
 *             description: Bad request
 *             content: 
 *                 application/json:
 *                     schema:
 *                         type: object
 */

/**
 * @swagger
 * /api/v1/products:
 *  delete:
 *      summary: Soft delete many products
 *      description: Get all product 
 *      tags: [Product]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        require: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *      responses:
 *          200: 
 *             description: Response success
 *             content: 
 *                 application/json:
 *                     schema:
 *                         type: object
 *          400:
 *             description: Bad request
 *             content: 
 *                 application/json:
 *                     schema:
 *                         type: object
 */

/**
 * @swagger
 * /api/v1/products/{id}:
 *  get:
 *      summary: Return a product
 *      description: Get a product by id
 *      tags: [Product]
 *      security: 
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: The product id
 *      responses:
 *          200:
 *            description: Response successful
 *            content:
 *              application/json:
 *                schema:         
 *                  type: object
 *          400:
 *              description: Bad request
 *              content:
 *                application/json:
 *                   schema:
 *                      type: object
 */

/**
 * @swagger
 * /api/v1/products/{id}:
 *  patch:
 *      summary: Update product
 *      description: Update a product by ids
 *      tags: [Product]
 *      security: 
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: The product id
 *      requestBody: 
 *          content:
 *            multipart/form-data:
 *              schema:
 *                $ref: '#/components/schemas/Product'
 *      responses:
 *          '200':
 *              description: update product successful
 *              content:
 *                  application/json:
 *                      schema:         
 *                          type: object
 *          '400':
 *              description: bad request
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 */

/**
 * @swagger
 * /api/v1/products/{id}:
 *  delete:
 *      summary: Delete a product
 *      description: Soft delete a product by id
 *      tags: [Product]
 *      security: 
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: The product id
 *      responses:
 *          200:
 *            description: Delete product successful
 *            content:
 *              application/json:
 *                schema:         
 *                  type: object
 *          400:
 *              description: Bad request
 *              content:
 *                application/json:
 *                   schema:
 *                      type: object
 */

/**
 * @swagger
 * /api/v1/products/sale:
 *   post:
 *     summary: Create a flash sale product
 *     tags: [Product]
 *     security:
 *        - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *              type: object
 *     responses:
 *       201:
 *         description: Create category successful
 *       500:
 *         description: Server error
 */

/**
 * @swagger
 * /api/v1/products/sale:
 *   get:
 *     summary: Create a flash sale product
 *     tags: [Product]
 *     security:
 *        - bearerAuth: []
 *     parameters:
 *        - in: query
 *          name: page
 *        - in: query
 *          name: size
 *     responses:
 *       200: 
 *          description: Response success
 *          content: 
 *              application/json:
 *                  schema:
 *                      type: object
 *       500:
 *         description: Server error
 *         content: 
 *             application/json:
 *                 schema:
 *                     type: object
 */