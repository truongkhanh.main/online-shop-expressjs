const jwt = require("jsonwebtoken")
const User = require('../model/user.model')
const { HTTP_CODE, MESSAGE, AUTHEN } = require('../helper/constants')
const Role = require("../model/role.model")
const RoleModules = require("../model/roleModules.model")
const UserRole = require("../model/userRole.model")

const verifyToken = (req, res, next) => {
    let token = req.headers["authorization"]
    if (!token || (token && token.split(' ').length < 2)) {
        return res.status(HTTP_CODE.UN_AUTH).json({ message: MESSAGE.UN_AUTH })
    }
    token = token.split(' ')[1]

    jwt.verify(token, process.env.SECRET_KEY, async function (err, decoded) {
        if (err) return res.status(HTTP_CODE.SERVER_ERROR).json({ auth: false, message: AUTHEN.AUTHEN_ERR, "Error": err })
        if (!decoded) return res.status(HTTP_CODE.SERVER_ERROR).json({ auth: false, message: AUTHEN.DECODED_ERR })
        const username = decoded.username
        const usernameWhere = { username: username }
        const account = await User.findOne({ where: usernameWhere })
        if (account) {
            req.userId = account.id
            req.token = token
            next()
        }
    })
}

const permission = (roleModule, api) => {
    return async (req, res, next) => {
        try {
            const userId = req.userId
            let dataCondition = { 'api': api || req.originalUrl }

            roleModule.forEach(element => {
                dataCondition[element] = 1
            })

            const userCondition = {
                where: { userId },
                include: {
                    model: Role,
                    right: true,
                    include: {
                        model: RoleModules,
                        where: dataCondition,
                        right: true   
                    }
                }
            }

            const userRole = await UserRole.findAll(userCondition)
            if (userRole.length > 0) {
                next()
            } else {
                throw new Error
            }
        } catch (error) {
            res.status(HTTP_CODE.NO_PERMISSION).json({ message: MESSAGE.NO_PERMISSION })
        }
    }
}

module.exports = { verifyToken, permission }

