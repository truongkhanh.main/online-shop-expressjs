const chai = require('chai')
const chaiHttp = require('chai-http')
const fs = require('fs')
const should = chai.should()
const server = require('../index')

const { HTTP_CODE, MESSAGE, DEFAULT_VALUES } = require('../helper/constants')

chai.use(chaiHttp)

// Get data return null => should.be.a('????')


// USER API
// describe('/GET User', () => {
//     let page = 1
//     let size = 5
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should get all user', (done) => {
//         chai.request(server)
//             .get('/api/v1/users?page=' + page + '&size=' + size)
//             .set('authorization', 'Bearer ' + token)
//             .end((err, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.data.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/GET user by id', () => {
//     let id = '8f04b729-09f1-4c27-9677-20d9de633072'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should get user by id', (done) => {
//         chai.request(server)
//             .get('/api/v1/users/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .end((err, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.data.should.be.a('object')
//                 done()
//             })
//     })
// })

// ORDER API
// describe('/GET order', () => {
//     let page = 1
//     let size = 5
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should get all order', (done) => {
//         chai.request(server)
//             .get('/api/v1/orders?page=' + page + '&size=' + size)
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.data.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/GET detail order', () => {
//     let id = '7d06d4dd-d895-441f-a238-3a7f3d90d029'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should get detail order', (done) => {
//         chai.request(server)
//             .get('/api/v1/orders/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.data.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/POST order', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     let order = {
//         orderCode: 'ORDERTEST',
//         listProductImport: [
//             { productId: '1fabe253-1559-4fc3-95ce-85e0b238a529', quantity: 10 },
//             { productId: '881c3261-34b8-4541-b6c4-b1203c341463', quantity: 1 },
//         ]
//     }
//     it('it should create a order', (done) => {
//         chai.request(server)
//             .post('/api/v1/orders')
//             .set('authorization', 'Bearer ' + token)
//             .send(order)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.CREATED_SUCCESS)
//                 res.body.should.be.a('object')
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.CREATED_SUCCESS)
//                 res.body.data.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/UPDATE-STATUS order', () => {
//     let id = 'a53ffe15-58fd-4f5b-9247-9ff58ee86c0e'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should update order status to confirm', (done) => {
//         chai.request(server)
//             .patch('/api/v1/orders/' + id + '/confirm')
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.body.should.be.a('object')
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 done()
//             })
//     })
// })

// describe('/UPDATE-STATUS order', () => {
//     let id = 'a53ffe15-58fd-4f5b-9247-9ff58ee86c0e'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should update order status to shipping', (done) => {
//         chai.request(server)
//             .patch('/api/v1/orders/' + id + '/shipping')
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.body.should.be.a('object')
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 done()
//             })
//     })
// })

// describe('/UPDATE-STATUS order', () => {
//     let id = 'a53ffe15-58fd-4f5b-9247-9ff58ee86c0e'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should update order status to delivered', (done) => {
//         chai.request(server)
//             .patch('/api/v1/orders/' + id + '/delivered')
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.body.should.be.a('object')
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 done()
//             })
//     })
// })

// describe('/UPDATE-STATUS order', () => {
//     let id = 'a53ffe15-58fd-4f5b-9247-9ff58ee86c0e'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should update order status to cancel', (done) => {
//         chai.request(server)
//             .patch('/api/v1/orders/' + id + '/cancel')
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.body.should.be.a('object')
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 done()
//             })
//     })
// })

// describe('/SOFT-DELETE order', () => {
//     let id = 'a53ffe15-58fd-4f5b-9247-9ff58ee86c0e'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should soft delete order', (done) => {
//         chai.request(server)
//             .delete('/api/v1/orders/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.body.should.be.a('object')
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.DELETED_SUCCESS)
//                 done()
//             })
//     })
// })

// PRODUCT API
// describe('/GET products', () => {
//     let page = 1
//     let size = 5
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should get all product', (done) => {
//         chai.request(server)
//             .get('/api/v1/products?page=' + page + '&size=' + size)
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.data.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/GET detail product', () => {
//     let id = '7ca9054f-ad7e-423d-b6c9-46bc046fba34'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should get detail product', (done) => {
//         chai.request(server)
//             .get('/api/v1/products/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.data.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/POST product', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should create a product', (done) => {
//         chai.request(server)
//             .post('/api/v1/products')
//             .set('authorization', 'Bearer ' + token)
//             .field('content-Type', 'multipart/form-data')
//             .field('name', 'May giat panasonic')
//             .field('description', 'May giat panasonic')
//             .field('barcode', 'MGPNSN')
//             .field('importPrice', '200')
//             .field('price', '150')
//             .field('tax', '10')
//             .field('quantity', '50')
//             .field('subcategoryId', '0c2760b3-e3e0-4be7-bfac-0e8f66dd4af0')
//             .attach('url', fs.readFileSync('H:/Ảnh/Comment Mèo Cute/15.jpg'), 'H:/Ảnh/Comment Mèo Cute/15.jpg')
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.CREATED_SUCCESS)
//                 res.body.should.be.a('object')
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.CREATED_SUCCESS)
//                 res.body.data.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/UPDATE product', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     let id = '285eeea2-2163-42a8-8e00-f1da05ebca0a'
//     it('it should create a product', (done) => {
//         chai.request(server)
//             .patch('/api/v1/products/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .field('content-Type', 'multipart/form-data')
//             .field('name', 'May giat samsung')
//             .field('description', 'May giat samsung')
//             .field('barcode', 'MGSS')
//             .field('importPrice', '260')
//             .field('price', '300')
//             .field('quantity', '50')
//             .field('subcategoryId', '0c2760b3-e3e0-4be7-bfac-0e8f66dd4af0')
//             .attach('url', fs.readFileSync('H:/Ảnh/Comment Mèo Cute/15.jpg'), 'H:/Ảnh/Comment Mèo Cute/15.jpg')
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/SOFT-DELETE product', () => {
//     let id = '285eeea2-2163-42a8-8e00-f1da05ebca0a'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should soft delete product', (done) => {
//         chai.request(server)
//             .delete('/api/v1/products/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.body.should.be.a('object')
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.DELETED_SUCCESS)
//                 done()
//             })
//     })
// })

// describe('/SOFT-DELETE many product', () => {
//     it('it should soft delete product', (done) => {
//         let data ={
//             listProductId : [
//             "1fabe253-1559-4fc3-95ce-85e0b238a529"
//             ]
//         }
//         let token = DEFAULT_VALUES.TOKEN
//         chai.request(server)
//             .delete('/api/v1/products')
//             .set('authorization', 'Bearer ' + token)
//             .send(data)
//             .end((req, res) => {
//                 res.body.should.be.a('object')
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.DELETED_SUCCESS)
//                 done();
//             })
//     })
// })

// describe('/POST flash sale product', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     let flashsaleproduct = {
//         productId: '285eeea2-2163-42a8-8e00-f1da05ebca0a',
//         flashsaleId: '4267e91c-a06a-4ac7-a62b-bc3414481e4b'
//     }
//     it('it should create a flash sale product', (done) => {
//         chai.request(server)
//             .post('/api/v1/products/sale')
//             .set('authorization', 'Bearer ' + token)
//             .send(flashsaleproduct)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.CREATED_SUCCESS)
//                 res.body.should.be.a('object')
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.CREATED_SUCCESS)
//                 res.body.data.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/GET flash sale product', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     let page = 1
//     let size = 2
//     it('it should get all flash sale product', (done) => {
//         chai.request(server)
//             .get('/api/v1/products/sale?page=' + page + '&size=' + size)
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// FLASH SALE API
// describe('/GET flash sale', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should get all flash sale', (done) => {
//         chai.request(server)
//             .get('/api/v1/flash-sales')
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/GET detail flash sale', () => {
//     let id = 'e3f18bbf-bf9e-4c86-8f30-7d2d55bba551'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should get detail flash sale', (done) => {
//         chai.request(server)
//             .get('/api/v1/flash-sales/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/GET list flash sale have no expire', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should get no expire flash sale', (done) => {
//         chai.request(server)
//             .get('/api/v1/flash-sales/no-expire')
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/POST flash sale', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     let flashsale = {
//         name: 'Flash sale 15/05',
//         description: 'Flash sale ngay 15 thang 5',
//         discountPercent: 20,
//         expireTime: 3
//     }
//     it('it should create a flash sale', (done) => {
//         chai.request(server)
//             .post('/api/v1/flash-sales')
//             .set('authorization', 'Bearer ' + token)
//             .send(flashsale)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.CREATED_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.CREATED_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/UPDATE voucher', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     let id = '4267e91c-a06a-4ac7-a62b-bc3414481e4b'
//     let flashsale = {
//         name: 'Flash sale 20/05',
//         description: 'Flash sale ngay 20 thang 5',
//         discountPercent: 10,
//         expireTime: 4
//     }
//     it('it should create a flash sale', (done) => {
//         chai.request(server)
//             .put('/api/v1/flash-sales/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .send(flashsale)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/SOFT-DELETE flash sale', () => {
//     let id = '4267e91c-a06a-4ac7-a62b-bc3414481e4b'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should soft delete flash sale', (done) => {
//         chai.request(server)
//             .patch('/api/v1/flash-sales/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.body.should.be.a('object')
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.DELETED_SUCCESS)
//                 done()
//             })
//     })
// })

// VOUCHER API
// describe('/GET vouchers', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should get all vouchers', (done) => {
//         chai.request(server)
//             .get('/api/v1/vouchers')
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/GET detail voucher', () => {
//     let id = '4fe9e530-60eb-412b-bf9c-32553c0e1038'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should get detail voucher', (done) => {
//         chai.request(server)
//             .get('/api/v1/vouchers/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/GET list voucher have no expire', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should get no expire voucher', (done) => {
//         chai.request(server)
//             .get('/api/v1/vouchers/no-expire')
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/POST voucher', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     let voucher = {
//         name: 'Voucher 15/05',
//         description: 'Voucher ngay 15 thang 5',
//         code: 'VOUCHER1505',
//         discountPercent: 10,
//         expireTime: 3
//     }
//     it('it should create a voucher', (done) => {
//         chai.request(server)
//             .post('/api/v1/vouchers')
//             .set('authorization', 'Bearer ' + token)
//             .send(voucher)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.CREATED_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.CREATED_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/UPDATE voucher', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     let id = '134e0db1-b396-43a8-9714-9fb2983365e8'
//     let voucher = {
//         name: 'Voucher 20/05',
//         description: 'Voucher ngay 20 thang 5',
//         code: 'VOUCHER2005',
//         discountPercent: 10,
//         expireTime: 4
//     }
//     it('it should create a voucher', (done) => {
//         chai.request(server)
//             .put('/api/v1/vouchers/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .send(voucher)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/SOFT-DELETE voucher', () => {
//     let id = '134e0db1-b396-43a8-9714-9fb2983365e8'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should soft delete voucher', (done) => {
//         chai.request(server)
//             .patch('/api/v1/vouchers/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.body.should.be.a('object')
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.DELETED_SUCCESS)
//                 done()
//             })
//     })
// })

// CATEGORY API
// describe('/GET categories', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should get all categories', (done) => {
//         chai.request(server)
//             .get('/api/v1/categories')
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/GET detail category', () => {
//     let id = 'f094adc6-10aa-4bb4-ab0c-d8b411ad0d98'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should get detail category', (done) => {
//         chai.request(server)
//             .get('/api/v1/categories/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/POST category', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should create a category', (done) => {
//         chai.request(server)
//             .post('/api/v1/categories/cate')
//             .set('authorization', 'Bearer ' + token)
//             .field('content-Type', 'multipart/form-data')
//             .field('name', 'Do dien tu')
//             .attach('image', fs.readFileSync('H:/Ảnh/Comment Mèo Cute/1.jpg'), 'H:/Ảnh/Comment Mèo Cute/1.jpg')
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.CREATED_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.CREATED_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/UPDATE category', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     let id = 'e56a5cdd-cc99-4e97-83f8-2efa1689f48c'
//     it('it should update a category', (done) => {
//         chai.request(server)
//             .put('/api/v1/categories/cate/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .field('content-Type', 'multipart/form-data')
//             .field('name', 'Do dien tu')
//             .attach('image', fs.readFileSync('H:/Ảnh/Comment Mèo Cute/2.jpg'), 'H:/Ảnh/Comment Mèo Cute/2.jpg')
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/SOFT-DELETE category', () => {
//     let id = '0a4ef213-aec2-4608-bd0b-bf97a8f25969'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should soft delete category', (done) => {
//         chai.request(server)
//             .patch('/api/v1/categories/cate/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.body.should.be.a('object')
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.DELETED_SUCCESS)
//                 done()
//             })
//     })
// })

// describe('/POST sub-category', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should create a sub-category', (done) => {
//         chai.request(server)
//             .post('/api/v1/categories/subcate')
//             .set('authorization', 'Bearer ' + token)
//             .field('content-Type', 'multipart/form-data')
//             .field('name', 'Tu lanh')
//             .field('categoryId', 'e56a5cdd-cc99-4e97-83f8-2efa1689f48c')
//             .attach('image', fs.readFileSync('H:/Ảnh/Comment Mèo Cute/5.jpg'), 'H:/Ảnh/Comment Mèo Cute/5.jpg')
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.CREATED_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.CREATED_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/UPDATE sub-category', () => {
//     let token = DEFAULT_VALUES.TOKEN
//     let id = '0c2760b3-e3e0-4be7-bfac-0e8f66dd4af0'
//     it('it should update a sub-category', (done) => {
//         chai.request(server)
//             .put('/api/v1/categories/subcate/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .field('content-Type', 'multipart/form-data')
//             .field('name', 'May giat')
//             .attach('image', fs.readFileSync('H:/Ảnh/Comment Mèo Cute/3.jpg'), 'H:/Ảnh/Comment Mèo Cute/3.jpg')
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/SOFT-DELETE sub category', () => {
//     let id = 'affbb19b-02d1-46c2-b368-83a4f22fa6a5'
//     let token = DEFAULT_VALUES.TOKEN
//     it('it should soft delete sub category', (done) => {
//         chai.request(server)
//             .patch('/api/v1/categories/subcate/' + id)
//             .set('authorization', 'Bearer ' + token)
//             .end((req, res) => {
//                 res.body.should.be.a('object')
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.DELETED_SUCCESS)
//                 done()
//             })
//     })
// })

// AUTH API
// describe('/POST login', () => {
//     let account = {
//         username: 'admin',
//         password: 'admin'
//     }
//     it('it should get detail category', (done) => {
//         chai.request(server)
//             .post('/api/v1/auth/login')
//             .send(account)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.RESPONSE_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.RESPONSE_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })

// describe('/POST register', () => {
//     let user = {
//         id: '0732ceb7-f167-40b7-ad67-dd3f0da57316',
//         username: 'tqk',
//         password: 'be0fc411c48d4ecc80a279a4cc625d71',
//         email: 'khanhtqhe130683@fpt.edu.vn',
//         name: 'Truong Quoc Khanh',
//         age: 22,
//         phone: '0379416224',
//         address: 'HN',
//         paymentMethod: 2,
//         verifyCode: 'HR7S65F6',
//         verifyStatus: 0,
//         isActive: 1
//     }
//     it('it should get detail category', (done) => {
//         chai.request(server)
//             .post('/api/v1/auth/register')
//             .send(user)
//             .end((req, res) => {
//                 res.should.have.status(HTTP_CODE.CREATED_SUCCESS)
//                 res.body.should.have.property('isSuccess').eql(true)
//                 res.body.should.have.property('message').eql(MESSAGE.CREATED_SUCCESS)
//                 res.body.should.be.a('object')
//                 done()
//             })
//     })
// })