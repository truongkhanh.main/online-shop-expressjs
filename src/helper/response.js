const { DEFAULT_VALUES } = require('../helper/constants')

class SuccessResponse {
    constructor(res, httpCode, message, data) {
        return res.status(httpCode).json({
            isSuccess: DEFAULT_VALUES.IS_SUCCESS,
            message,
            data
        })
    }
}

class ErrorResponse {
    constructor(res, httpCode, errorCode, message) {
        return res.status(httpCode).json({
            isSuccess: DEFAULT_VALUES.IS_NOT_SUCCESS,
            errorCode,
            message
        })
    }
}

module.exports = { SuccessResponse, ErrorResponse }