const nodemailer = require("nodemailer")
const { DEFAULT_VALUES } = require('./constants')

module.exports = {

    getPagination: (page, size) => {
        const limit = size ? +size : parseInt(DEFAULT_VALUES.DEFAULT_SIZE)
        const offset = page ? (page - 1) * limit : parseInt(DEFAULT_VALUES.DEFAULT_OFFSET)
        return { limit, offset }
    },

    totalPriceProduct: (price, tax) => {
        price = parseInt(price)
        tax = parseInt(tax)
        return price + (price * tax / 100)
    },

    priceAfterSale: (price, discount) => {
        price = parseInt(price)
        discount = parseInt(discount)
        return price * (100 - discount) / 100
    },

    quantityStock: (original, sold) => {
        original = parseInt(original)
        sold = parseInt(sold)
        return original - sold
    },

    recoveryStock: (original, sold) => {
        original = parseInt(original)
        sold = parseInt(sold)
        return original + sold
    },

    sendEmailVerify: (email, host, username, password, verifyCode) => {
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.FROM_EMAIL,
                pass: process.env.FROM_EMAIL_PASSWORD
            },
        })
        // send mail with defined transport object
        let mailOptions = {
            from: process.env.FROM_EMAIL,
            to: email,
            subject: 'Sending Email using Node.js',
            html: `<div><b>Account:</b> ${username}</div>
            <div><b>Password:</b> ${password}</div>
            <div><p>Please verify your account:</p></div>
            <div><a href='http://${host}/api/v1/auth/verify/${username}/${verifyCode}'>VERIFY</a></div>`, // html body
        }

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                throw new Error
            } else {
                return
            }
        })
    },

    sendEmailCronjob: (email, text, html) => {
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.FROM_EMAIL,
                pass: process.env.FROM_EMAIL_PASSWORD
            },
        })
        // send mail with defined transport object
        let mailOptions = {
            from: process.env.FROM_EMAIL,
            to: email,
            subject: 'Sending Email using Node.js',
            text: text,
            html: html
        }

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                throw new Error
            } else {
                return
            }
        })
    },

    sendEmailForgotPassword: (email, newPassword) => {
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.FROM_EMAIL,
                pass: process.env.FROM_EMAIL_PASSWORD
            },
        })
        // send mail with defined transport object
        let mailOptions = {
            from: process.env.FROM_EMAIL,
            to: email,
            subject: 'Sending Email using Node.js',
            text: `Your new password is ${newPassword}. Please change your password`
        }

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                throw new Error
            } else {
                return
            }
        })
    },

    generateVerifyCode: (verifyCharacter, length) => {
        let result = ''
        for (var i = 0; i < length; i++) {
            result += verifyCharacter.charAt(Math.floor(Math.random() * verifyCharacter.length));
        }
        return result
    }
}