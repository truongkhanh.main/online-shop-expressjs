module.exports = {
    HTTP_CODE: {
        RESPONSE_SUCCESS: 200,
        CREATED_SUCCESS: 201,
        DELETED_SUCCESS: 204,
        BAD_REQUEST: 400,
        UN_AUTH: 401,
        PAGE_NOT_FOUND: 404,
        NO_PERMISSION: 403,
        SERVER_ERROR: 500
    },

    ERROR_CODE: {
        BAD_REQUEST: 'BAD_REQUEST',
        UN_AUTH: 'UN_AUTHENTICATION',
        PAGE_NOT_FOUND: 'PAGE_NOT_FOUND',
        NO_PERMISSION: 'NO_PERMISSION',
        SERVER_ERROR: 'SERVER_ERROR'
    },

    MESSAGE: {
        CONNECT_SUCCESS: "Connection successfully",
        RESPONSE_SUCCESS: "Successfull",
        CREATED_SUCCESS: "Create successfully",
        DELETED_SUCCESS: "Delete successfully",
        BAD_REQUEST: "Bad request",
        UN_AUTH: "Required authentication",
        PAGE_NOT_FOUND: "Page not found",
        NO_PERMISSION: "User don't have permission",
        SERVER_ERROR: "Server error",
        EXISTS_USER: "Username existed",
        EXISTS_EMAIL: "Email existed",
        LOGIN_FAIL: "Login fail",
        VERIFY_SUCCESS: "Account verify successful",
        VERIFY_FAIL: "Account verify fail",
        VERIFY_TIME_UNEXPIRED: "Link verify unexpired",
        USER_VERIFIED: "User have verified",
        REQUEST_VERIFY: "Account not verify. Please resend email to verify",
        SEND_MAIL_SUCCESS: "Sent verify mail successfull",
        CANNOT_DELETE: "Can't delete",
        PRODUCT_EXISTED: "Product existed flash sale",
        SALE_EXPIRE: "Flash sale ended",
        CANNOT_CANCEL: "Can't cancel order",
        CANNOT_UPDATE_ORDER: "Can't change order status",
        OVER_QUANTITY: "Over allowed quantity",
        NO_DELETE_PRODUCT: "Product was in some order, cannot delete this product",
        OLD_PASSWORD_WRONG: "Your password is incorrect",
        NEW_PASSWORD_WRONG: "Re-new password is incorrect"
    },

    DEFAULT_VALUES: {
        DEFAULT_USER_ROLE: 'user',
        DEFAULT_ADMIN_ROLE: 'admin',
        DEFAULT_OFFSET: 0,
        DEFAULT_PAGE: 1,
        DEFAULT_SIZE: 10,
        ASC_SORT: 'ASC',
        DESC_SORT: 'DESC',
        IS_SUCCESS: true,
        IS_NOT_SUCCESS: false,
        PAGE_INDEX: 1,
        IS_DELETE: 1,
        IS_ACTIVE: 1,
        IS_NOT_DELETE: 0,
        IS_NOT_ACTIVE: 0,
        VOUCHER: null,
        PRODUCT_TAX: 10,
        TOKEN: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjhmMDRiNzI5LTA5ZjEtNGMyNy05Njc3LTIwZDlkZTYzMzA3MiIsInVzZXJuYW1lIjoiYWRtaW4iLCJpYXQiOjE2NTI0MjU5NzUsImV4cCI6MTY1MjQzMzE3NX0._qqcMqhpfMrdWCGeAXjUECvxV_IqUkqusaYP5csdSM0'
    },

    AUTHEN: {
        AUTHEN_ERR: "Fail to authenticate token.",
        DECODED_ERR: "Not found user"
    },

    VERIFY: {
        VERIFY_STATUS: 1,
        NOT_VERIFY_STATUS: 0,
        VERIFY_TIME: 15,
        VERIFY_CHARACTER: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
        VERIFY_CODE_LENGTH: 8
    },

    PERMISSION: {
        READ: 'isCanRead',
        ADD: 'isCanAdd',
        UPDATE: 'isCanEdit',
        DELETE: 'isCanDelete'
    },

    API: {
        VOUCHER: '/api/v1/vouchers/:id',
        SUBCATE: '/api/v1/categories/subcate/:id',
        CATE: '/api/v1/categories/cate/:id',
        FLASH_SALE: '/api/v1/flash-sales/:id',
        PRODUCT: '/api/v1/products/:id',
        ORDER: '/api/v1/orders/:id',
        ORDER_CONFIRM: '/api/v1/orders/:id/confirm',
        ORDER_SHIPPING: '/api/v1/orders/:id/shipping',
        ORDER_DELIVERED: '/api/v1/orders/:id/delivered',
        ORDER_CANCEL: '/api/v1/orders/:id/cancel',
        USERS: '/api/v1/users',
        USER: '/api/v1/users/:id'
    },

    STATUS_ORDER: {
        CREATED: 1,
        CONFIRMED: 2,
        SHIPPING: 3,
        DELIVERIED: 4,
        CANCELED: 5
    },

    EMAIL_CONTENT: {
        EMAIL_TEXT_VERIFY: 'You have successfully created an account and here is your account: ',
        EMAIL_TEXT_FLASHSALE: 'The flash sale goes on in 15 minutes'
    }

}