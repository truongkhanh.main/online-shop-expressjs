const cron = require('node-cron')
const moment = require('moment')

const { DEFAULT_VALUES, EMAIL_CONTENT } = require('../helper/constants')
const User = require('../model/User')
const FlashSale = require('../model/FlashSale')
const { sendEmail } = require('../helper/helper')

const task = cron.schedule('*/1 * * * *', async () => {
    const flashSaleCondition = {
        where: {
            isDeleted: DEFAULT_VALUES.IS_NOT_DELETE,
            startDate: moment().add(15, 'm')
        }
    }

    const flashsale = await FlashSale.findAll(flashSaleCondition)

    if (flashsale.length > 0) {
        const userCondition = {
            where: { isActive: DEFAULT_VALUES.IS_ACTIVE }
        }
        const users = await User.findAll(userCondition)
        if (users.length > 0) {
            for (const user of users) {
                const text = EMAIL_CONTENT.EMAIL_TEXT_FLASHSALE
                sendEmailCronjob(user.email, text, null)
            }
        }
    }
})

module.exports = task