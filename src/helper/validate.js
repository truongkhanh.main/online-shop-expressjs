module.exports = {
    emailValidate: function (email) {
        const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        return email.match(regex)
    },

    phoneValidate: function (phone) {
        const regex = /(0[3|5|7|8|9])+([0-9]{8})\b/g
        return phone.match(regex)
    }
}