const md5 = require('md5')
const jwt = require("jsonwebtoken")

const { sequelize } = require('../config/database')
const { HTTP_CODE, MESSAGE, DEFAULT_VALUES, ERROR_CODE } = require('../helper/constants')
const { getPagination } = require('../helper/helper')
const { SuccessResponse, ErrorResponse } = require('../helper/response')
const User = require('../model/user.model')
const Customer = require('../model/customer.model')

class UserController {

    listUser = async (req, res) => {
        try {
            let { page, size } = req.query
            if (!Number.isInteger(+page)) {
                page = DEFAULT_VALUES.DEFAULT_PAGE
            }
            if (!Number.isInteger(+size)) {
                size = DEFAULT_VALUES.DEFAULT_SIZE
            }

            const { limit, offset } = getPagination(page, size)

            const userCondition = { isActive: DEFAULT_VALUES.IS_ACTIVE }
            const includeModel = [{ model: Customer }]

            const users = await User.findAndCountAll({
                limit,
                offset,
                where: userCondition,
                include: includeModel
            })

            // RESPONSE
            const dataResponse = {
                pageIndex: parseInt(page) || DEFAULT_VALUES.PAGE_INDEX,
                currentPageSize: users.rows.length,
                totalPage: Math.ceil(users.count / size),
                totalSize: users.count,
                data: users.rows
            }

            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, dataResponse)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    getOne = async (req, res) => {
        const userId = req.params.id
        if (!userId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        try {
            const userCondition = { id: userId }
            const includeModel = [{ model: Customer }]
            
            const user = await User.findOne({
                where: userCondition,
                include: includeModel
            })
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, user)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    updateInfo = async (req, res) => {
        try {
            const token = req.token
            const verifyToken = jwt.verify(token, process.env.SECRET_KEY)

            const userId = req.params.id
            if (!userId || userId !== verifyToken.id) {
                return ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }
            const data = req.body

            const userCondition = { where: { id: userId } }
            const customerCondition = { where: { userId } }
            const dataUpdate = {
                ...data,
                updatedBy: verifyToken.id
            }

            const checkUser = await User.findOne(userCondition)
            if (!checkUser) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            await Customer.update(
                dataUpdate,
                customerCondition
            )
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    inactive = async (req, res) => {
        const userId = req.params.id
        if (!userId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        const transaction = await sequelize.transaction()
        try {
            const userCondition = { where: { id: userId }, transaction }
            const customerCondition = { where: { userId }, transaction }
            const dataUpdate = { isActive: DEFAULT_VALUES.IS_NOT_ACTIVE }

            const checkUser = await User.findOne(userCondition)
            if (!checkUser) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            const userUpdate = await User.update(
                dataUpdate,
                userCondition
            )
            const customerUpdate = await Customer.update(
                dataUpdate,
                customerCondition
            )
            if (userUpdate && customerUpdate) {
                await transaction.commit()
                new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.DELETED_SUCCESS)
            }
        } catch (error) {
            await transaction.rollback()
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }
}

module.exports = new UserController