const md5 = require('md5')
const uuid = require('uuid')
const jwt = require("jsonwebtoken")
const moment = require('moment')

const { sequelize } = require('../config/database')
const { HTTP_CODE, MESSAGE, DEFAULT_VALUES, VERIFY, ERROR_CODE } = require('../helper/constants')
const { generateVerifyCode, sendEmailForgotPassword, sendEmailVerify } = require('../helper/helper')
const { emailValidate, phoneValidate } = require('../helper/validate')
const { SuccessResponse, ErrorResponse } = require('../helper/response')
const User = require('../model/user.model')
const Customer = require('../model/customer.model')
const UserRole = require('../model/userRole.model')
const Role = require('../model/role.model')
const RoleModules = require('../model/roleModules.model')

class UserController {

    register = async (req, res) => {
        const { username, password, name, age, email, phone, address, paymentMethod } = req.body
        if (!username || !password || !name || !email || !phone || !address || !paymentMethod) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }
        if (!emailValidate(email)) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }
        if (!phoneValidate(phone)) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }
        if (age > 120 || age < 15) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        const transaction = await sequelize.transaction()
        try {
            const verifyCharacter = VERIFY.VERIFY_CHARACTER
            const length = VERIFY.VERIFY_CODE_LENGTH
            const verifyCode = generateVerifyCode(verifyCharacter, length)

            const usernameCondition = { where: { username } }
            const checkUsername = await User.findOne(usernameCondition)
            if (checkUsername) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.EXISTS_USER)
            }

            const emailCondition = { where: { email } }
            const checkEmail = await User.findOne(emailCondition)
            if (checkEmail) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.EXISTS_EMAIL)
            }

            const hashedPassword = md5(password)

            const verifyTime = moment().add(VERIFY.VERIFY_TIME, 'm')

            const userData = {
                id: uuid.v4(),
                username,
                password: hashedPassword,
                email,
                verifyCode,
                verifyStatus: VERIFY.NOT_VERIFY_STATUS,
                verifyTime,
                isActive: DEFAULT_VALUES.IS_NOT_ACTIVE
            }

            const userCreated = await User.create(userData, { transaction })

            const customerData = {
                id: uuid.v4(),
                userId: userCreated.id,
                name,
                age,
                phone,
                address,
                paymentMethod,
                isActive: DEFAULT_VALUES.IS_NOT_ACTIVE
            }

            const customerCreated = await Customer.create(customerData, { transaction })

            const role = await Role.findOne({ where: { name: DEFAULT_VALUES.DEFAULT_USER_ROLE } })

            const userRoleData = {
                id: uuid.v4(),
                name: role.name,
                description: role.description,
                userId: userCreated.id,
                roleId: role.id
            }
            const userRoleCreated = await UserRole.create(userRoleData, { transaction })
            if (userCreated && customerCreated && userRoleCreated) {
                const host = req.headers.host
                sendEmailVerify(email, host, username, password, verifyCode)
                await transaction.commit()

                // RESPONSE
                const dataResponse = {
                    userCreated,
                    customerCreated
                }
                new SuccessResponse(res, HTTP_CODE.CREATED_SUCCESS, MESSAGE.CREATED_SUCCESS, dataResponse)
            } else {
                throw new Error
            }
        } catch (error) {
            await transaction.rollback()
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    verify = async (req, res) => {
        const transaction = await sequelize.transaction()
        try {
            const userVerifyCondition = { where: { username: req.params.username }, transaction }
            const userVerify = await User.findOne(userVerifyCondition)

            if (moment().isBefore(userVerify.verifyTime)) {
                const verifyCode = req.params.verifyCode
                if (!verifyCode) {
                    return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
                }

                if (verifyCode === userVerify.verifyCode) {
                    const dataUpdate = {
                        verifyStatus: VERIFY.VERIFY_STATUS,
                        isActive: DEFAULT_VALUES.IS_ACTIVE
                    }

                    await User.update(dataUpdate, userVerifyCondition)

                    const customerCondition = { where: { userId: userVerify.id }, transaction }
                    await Customer.update(dataUpdate, customerCondition)

                    transaction.commit()
                    new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.VERIFY_SUCCESS)
                } else {
                    throw new Error
                }
            } else {
                throw new Error
            }
        } catch (error) {
            transaction.rollback()
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    login = async (req, res) => {
        try {
            const { username, password } = req.body
            if (!(username && password)) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            const userCondition = { where: { username } }
            let account = await User.findOne(userCondition)

            // In case user have not verify
            if (!account.verifyStatus) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.REQUEST_VERIFY)
            } else {
                // In case user verified
                const hasPassword = md5(password)
                if (hasPassword === account.password) {
                    const expiresIn = { expiresIn: process.env.EXPIRES_IN }
                    const token = jwt.sign(
                        { id: account.id, username: account.username },
                        process.env.SECRET_KEY,
                        expiresIn
                    )

                    // RESPONSE
                    const dataResponse = {
                        token,
                        account
                    }
                    new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, dataResponse)
                }
            }
        }
        catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    resendEmail = async (req, res) => {
        try {
            const { email } = req.body
            const userCondition = { where: { email } }
            let account = await User.findOne(userCondition)

            if (account.verifyStatus) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.USER_VERIFIED)
            }

            if (moment().isBefore(account.verifyTime)) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.VERIFY_TIME_UNEXPIRED)
            }

            const verifyTime = moment().add(VERIFY.VERIFY_TIME, 'm')
            const verifyCode = generateVerifyCode(VERIFY.VERIFY_CHARACTER, VERIFY.VERIFY_CODE_LENGTH)
            const userVerifyCondition = { where: { username: account.username } }
            const userData = {
                verifyTime,
                verifyCode
            }

            await User.update(userData, userVerifyCondition)

            const host = req.headers.host
            sendEmailVerify(account.email, host, username, password, verifyCode)
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    forgotPassword = async (req, res) => {
        try {
            const { email } = req.body
            const userCondition = { where: { email } }
            const user = await User.findOne(userCondition)
            if (!user) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }
            const newRandomPassword = generateVerifyCode(VERIFY.VERIFY_CHARACTER, 10)

            const hasPassword = md5(newRandomPassword)
            const dataUpdate = { password: hasPassword }

            const userUpdated = await User.update(dataUpdate, userCondition)
            if (userUpdated) {
                sendEmailForgotPassword(email, newRandomPassword)
                new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS)
            }
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    changePassword = async (req, res) => {
        const userId = req.userId
        const { oldPassword, newPassword, renewPassword } = req.body
        if (!oldPassword || !newPassword || !renewPassword) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        try {
            const userCondition = { where: { id: userId } }
            const user = await User.findOne(userCondition)
            const hasPassword = md5(oldPassword)

            if (hasPassword !== user.password) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.OLD_PASSWORD_WRONG)
            }
            if (newPassword !== renewPassword) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.NEW_PASSWORD_WRONG)
            }

            const hasNewPassword = md5(newPassword)
            const dataUpdate = { password: hasNewPassword }

            await User.update(dataUpdate, userCondition)
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    rolemodule = async (req, res) => {
        const data = {
            id: uuid.v4(),
            api: '/api/v1/users/:id',
            isCanRead: false,
            isCanAdd: false,
            isCanEdit: false,
            isCanDelete: true,
            roleId: '75442486-0878-440c-9db1-a7006c25a39f'
        }

        await RoleModules.create(data)
        res.json({ message: MESSAGE.CREATED_SUCCESS })
    }
}

module.exports = new UserController
