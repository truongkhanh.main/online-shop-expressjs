const uuid = require('uuid')
const jwt = require('jsonwebtoken')
const { Op } = require('sequelize')
const moment = require('moment')

const { sequelize } = require('../config/database')
const Product = require('../model/product.model')
const FlashSaleProduct = require('../model/flashSaleProduct.model')
const Voucher = require('../model/voucher.model')
const Order = require('../model/order.model')
const OrderDetail = require('../model/orderDetail.model')
const Customer = require('../model/customer.model')
const { HTTP_CODE, MESSAGE, DEFAULT_VALUES, STATUS_ORDER, ERROR_CODE } = require('../helper/constants')
const { SuccessResponse, ErrorResponse } = require('../helper/response')
const { priceAfterSale, quantityStock, recoveryStock, getPagination } = require('../helper/helper')

class OrderController {

    getAll = async (req, res) => {
        try {
            let { page, size } = req.query
            if (!Number.isInteger(+page)) {
                page = DEFAULT_VALUES.DEFAULT_PAGE
            }
            if (!Number.isInteger(+size)) {
                size = DEFAULT_VALUES.DEFAULT_SIZE
            }

            const { limit, offset } = getPagination(page, size)

            const userId = req.userId
            const customer = await Customer.findOne({ where: { userId } })
            const customerId = customer.id
            if (!customerId) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            const orderCondition = {
                limit,
                offset,
                where: {
                    isDeleted: DEFAULT_VALUES.IS_NOT_DELETE,
                    customerId
                },
                order: [['createdAt', DEFAULT_VALUES.DESC_SORT]]
            }

            const orders = await Order.findAndCountAll(orderCondition)

            // RESPONSE
            const dataResponse = {
                pageIndex: parseInt(page) || DEFAULT_VALUES.PAGE_INDEX,
                currentPageSize: orders.rows.length,
                totalPage: Math.ceil(orders.count / size),
                totalSize: orders.count,
                data: orders.rows
            }
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, dataResponse)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    getOne = async (req, res) => {
        try {
            const orderId = req.params.id
            if (!orderId) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            const userId = req.userId
            const customer = await Customer.findOne({ where: { userId } })
            if (!customer) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            const orderCondition = {
                where: {
                    id: orderId,
                    isDeleted: DEFAULT_VALUES.IS_NOT_DELETE,
                    customerId: customer.id
                },
                include: {
                    model: OrderDetail
                }
            }

            const order = await Order.findOne(orderCondition)
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, order)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    create = async (req, res) => {
        const transaction = await sequelize.transaction()
        try {
            let { orderCode, voucherId, listProductImport } = req.body
            if (!orderCode || !listProductImport) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            const userId = req.userId
            const customer = await Customer.findOne({ where: { userId } })
            const customerId = customer.id
            if (!customerId) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            // Sort array by productId
            listProductImport.sort((a, b) => {
                if (a.productId < b.productId) {
                    return -1
                }
                if (a.productId < b.productId) {
                    return 1
                }
                return 0
            })

            const listProductId = listProductImport.map(productItem => {
                return productItem.productId
            })

            const listProduct = await Product.findAll({
                where: {
                    id: listProductId
                },
                include: {
                    model: FlashSaleProduct,
                    where: {
                        endDate: {
                            [Op.gt]: moment()
                        }
                    },
                    required: false
                }
            })

            const token = req.token
            const verifyToken = jwt.verify(token, process.env.SECRET_KEY)

            let totalPrice = 0
            let dataOrderDetail = []
            const orderId = uuid.v4()

            // Get total price of order && Create data order detail
            for (const index in listProduct) {
                // Check amount of products
                if (listProductImport[index].quantity > listProduct[index].allowedQuantity) {
                    return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.OVER_QUANTITY)
                }
                let priceSale = 0
                // Check product have flash sale or not
                if (listProduct[index].flashsaleproducts.length > 0) {
                    const sale = listProduct[index].flashsaleproducts[0].discountPercent
                    priceSale = priceAfterSale(listProduct[index].price, sale)
                } else {
                    priceSale = listProduct[index].price
                }
                totalPrice += priceSale * listProductImport[index].quantity

                // Create data order detail
                const data = {
                    id: uuid.v4(),
                    orderId,
                    productId: listProductImport[index].productId,
                    quantity: listProductImport[index].quantity,
                    price: priceSale,
                    createdBy: verifyToken.id,
                    isDeleted: DEFAULT_VALUES.IS_NOT_DELETE
                }
                dataOrderDetail.push(data)
            }

            // Check order have voucher or not
            if (voucherId) {
                const voucherCondition = {
                    where: {
                        id: voucherId,
                        endDate: {
                            [Op.gt]: moment()
                        },
                        quantity: {
                            [Op.gt]: 0
                        }
                    }
                }
                const voucher = await Voucher.findOne(voucherCondition)
                if (!voucher) {
                    return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
                }
                const discountPercent = voucher.discountPercent
                totalPrice = priceAfterSale(totalPrice, discountPercent)

                // Minus quantity voucher
                const dataVoucher = { quantity: voucher.quantity - 1 }
                await Voucher.update(dataVoucher, voucherCondition)
            }

            const dataOrder = {
                id: orderId,
                orderCode,
                status: STATUS_ORDER.CREATED,
                totalPrice,
                customerId,
                voucherId,
                isDeleted: DEFAULT_VALUES.IS_NOT_DELETE,
                createdBy: verifyToken.id
            }
            const orderCreated = await Order.create(dataOrder, { transaction })
            const orderDetailCreated = await OrderDetail.bulkCreate(dataOrderDetail, { transaction })

            if (orderCreated && orderDetailCreated) {
                // Minus quantity product
                for (const orderDetail of dataOrderDetail) {
                    const productCondition = { where: { id: orderDetail.productId }, transaction }
                    const product = await Product.findOne(productCondition)
                    const allowedQuantity = quantityStock(product.allowedQuantity, orderDetail.quantity)

                    const dataProduct = { allowedQuantity }
                    await Product.update(dataProduct, productCondition)
                }
            }

            // RESPONSE
            const dataResponse = {
                orderCreated,
                orderDetailCreated
            }

            await transaction.commit()
            new SuccessResponse(res, HTTP_CODE.CREATED_SUCCESS, MESSAGE.CREATED_SUCCESS, dataResponse)
        } catch (error) {
            await transaction.rollback()
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    updateConfirmStatus = async (req, res) => {
        try {
            const orderId = req.params.id
            if (!orderId) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            const orderCondition = { where: { id: orderId } }
            const order = await Order.findOne(orderCondition)
            if (order.status !== STATUS_ORDER.CREATED) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.CANNOT_UPDATE_ORDER)
            }

            const dataUpdate = { status: STATUS_ORDER.CONFIRMED }

            await Order.update(dataUpdate, orderCondition)
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    updateShippingStatus = async (req, res) => {
        try {
            const orderId = req.params.id
            if (!orderId) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            const orderCondition = { where: { id: orderId } }
            const order = await Order.findOne(orderCondition)
            if (order.status !== STATUS_ORDER.CONFIRMED) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.CANNOT_UPDATE_ORDER)
            }

            const dataUpdate = { status: STATUS_ORDER.SHIPPING }

            await Order.update(dataUpdate, orderCondition)
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    updateDeliveredStatus = async (req, res) => {
        try {
            const orderId = req.params.id
            if (!orderId) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            const orderCondition = { where: { id: orderId } }
            const order = await Order.findOne(orderCondition)
            if (order.status !== STATUS_ORDER.SHIPPING) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.CANNOT_UPDATE_ORDER)
            }

            const dataUpdate = { status: STATUS_ORDER.DELIVERIED }

            await Order.update(dataUpdate, orderCondition)

            // Minus actual quantity
            const orderDetailCondition = { where: { orderId } }
            const orderDetails = await OrderDetail.findAll(orderDetailCondition)
            for (const orderDetail of orderDetails) {
                const productCondition = { where: { id: orderDetail.productId } }
                const product = await Product.findOne(productCondition)
                const dataProductUpdate = {
                    actualQuantity: quantityStock(product.actualQuantity, orderDetail.quantity)
                }
                await Product.update(dataProductUpdate, productCondition)
            }
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    cancelOrder = async (req, res) => {
        try {
            const orderId = req.params.id
            if (!orderId) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }
            const orderCondition = { where: { id: orderId } }
            const order = await Order.findOne(orderCondition)
            if (order.status !== STATUS_ORDER.CREATED && order.status !== STATUS_ORDER.CONFIRMED) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.CANNOT_CANCEL)
            }

            const dataUpdate = { status: STATUS_ORDER.CANCELED }

            await Order.update(dataUpdate, orderCondition)

            // Add allowed quantity
            const orderDetailCondition = { where: { orderId } }
            const orderDetails = await OrderDetail.findAll(orderDetailCondition)
            for (const orderDetail of orderDetails) {
                const productCondition = { where: { id: orderDetail.productId } }
                const product = await Product.findOne(productCondition)
                const dataProductUpdate = {
                    allowedQuantity: recoveryStock(product.allowedQuantity, orderDetail.quantity)
                }
                await Product.update(dataProductUpdate, productCondition)
            }
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    delete = async (req, res) => {
        const transaction = await sequelize.transaction()
        try {
            const orderId = req.params.id
            if (!orderId) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            const checkOrder = await Order.findOne({ where: { id: orderId } })
            if (!checkOrder) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }
            if (!checkOrder.status !== 5) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.CANNOT_DELETE)
            }

            const dataDetele = {
                isDeleted: DEFAULT_VALUES.IS_DELETE
            }

            const orderCondition = { where: { id: orderId }, transaction }
            const orderDetailCondition = { where: { orderId }, transaction }

            await Order.update(dataDetele, orderCondition)
            await OrderDetail.update(dataDetele, orderDetailCondition)

            transaction.commit()
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.DELETED_SUCCESS)
        } catch (error) {
            transaction.rollback()
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }
}

module.exports = new OrderController