const uuid = require('uuid')
const moment = require('moment')
const jwt = require("jsonwebtoken")
const { Op } = require('sequelize')

const { HTTP_CODE, MESSAGE, DEFAULT_VALUES, ERROR_CODE } = require('../helper/constants')
const { SuccessResponse, ErrorResponse } = require('../helper/response')
const { getPagination } = require('../helper/helper')
const FlashSale = require('../model/flashSale.model')


class FlashSaleController {
    create = async (req, res) => {
        const { name, description, discountPercent, startTime, expireTime } = req.body
        if (!name || !description || !discountPercent || !expireTime) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        try {
            const startDate = startTime || moment(new Date()).add(20, 'm')
            const endDate = moment(startDate).add(expireTime, 'h')

            const token = req.token
            const verifyToken = jwt.verify(token, process.env.SECRET_KEY)

            const dataFlashSale = {
                id: uuid.v4(),
                name,
                description,
                discountPercent,
                startDate,
                endDate,
                isDeleted: DEFAULT_VALUES.IS_NOT_DELETE,
                createdBy: verifyToken.id,
                updatedBy: verifyToken.id
            }

            const flashSaleCreated = await FlashSale.create(dataFlashSale)
            new SuccessResponse(res, HTTP_CODE.CREATED_SUCCESS, MESSAGE.CREATED_SUCCESS, flashSaleCreated)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }

    }

    getAll = async (req, res) => {
        try {
            let { page, size } = req.query
            if (!Number.isInteger(+page)) {
                page = DEFAULT_VALUES.DEFAULT_PAGE
            }
            if (!Number.isInteger(+size)) {
                size = DEFAULT_VALUES.DEFAULT_SIZE
            }

            const { limit, offset } = getPagination(page, size)

            const flashSaleCondition = {
                limit,
                offset,
                where: {
                    isDeleted: DEFAULT_VALUES.IS_NOT_DELETE
                }
            }
            const flashsales = await FlashSale.findAndCountAll(flashSaleCondition)

            // RESPONSE
            const dataResponse = {
                pageIndex: parseInt(page) || DEFAULT_VALUES.PAGE_INDEX,
                currentPageSize: flashsales.rows.length,
                totalPage: Math.ceil(flashsales.count / size),
                totalSize: flashsales.count,
                data: flashsales.rows
            }
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, dataResponse)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    getNoExpire = async (req, res) => {
        try {
            let { page, size } = req.query
            if (!Number.isInteger(+page)) {
                page = DEFAULT_VALUES.DEFAULT_PAGE
            }
            if (!Number.isInteger(+size)) {
                size = DEFAULT_VALUES.DEFAULT_SIZE
            }

            const { limit, offset } = getPagination(page, size)

            const flashSaleCondition = {
                limit,
                offset,
                where: {
                    isDeleted: DEFAULT_VALUES.IS_NOT_DELETE,
                    endDate: {
                        [Op.gt]: moment()
                    }
                }
            }

            const flashsale = await FlashSale.findAndCountAll(flashSaleCondition)

            // RESPONSE
            const dataResponse = {
                pageIndex: parseInt(page) || DEFAULT_VALUES.PAGE_INDEX,
                currentPageSize: flashsale.rows.length,
                totalPage: Math.ceil(flashsale.count / size),
                totalSize: flashsale.count,
                data: flashsale.rows
            }
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, dataResponse)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    getOne = async (req, res) => {
        const flashsaleId = req.params.id
        if (!flashsaleId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        try {
            const flashSaleCondition = { where: { id: flashsaleId } }
            const flashsale = await FlashSale.findOne(flashSaleCondition)
            if (!flashsale) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, flashsale)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    update = async (req, res) => {
        try {
            const flashsaleId = req.params.id
            if (!flashsaleId) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }
            const oldFlashSale = await FlashSale.findOne({ where: { id: flashsaleId } })
            if (!oldFlashSale) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            let data = req.body

            const startDate = data.startTime || oldFlashSale.startDate
            const endDate = (data.expireTime) ? moment(startDate).add(data.expireTime, 'h') : oldFlashSale.endDate

            const token = req.token
            const verifyToken = jwt.verify(token, process.env.SECRET_KEY)

            const dataUpdate = {
                ...data,
                startDate,
                endDate,
                isDeleted: DEFAULT_VALUES.IS_NOT_DELETE,
                createdBy: oldFlashSale.createdBy,
                updatedBy: verifyToken.id
            }
            const updateCondition = { where: { id: flashsaleId } }

            await FlashSale.update(dataUpdate, updateCondition)
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    delete = async (req, res) => {
        const flashsaleId = req.params.id
        if (!flashsaleId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        try {
            const dataDetele = {
                isDeleted: DEFAULT_VALUES.IS_DELETE
            }
            const deleteCondition = { where: { id: flashsaleId } }
            const checkFlashsale = await FlashSale.findOne(deleteCondition)
            if (!checkFlashsale) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }
            
            await FlashSale.update(dataDetele, deleteCondition)
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.DELETED_SUCCESS)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }
}

module.exports = new FlashSaleController
