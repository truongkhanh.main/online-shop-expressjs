const uuid = require('uuid')
const moment = require('moment')
const jwt = require("jsonwebtoken")

const { HTTP_CODE, MESSAGE, DEFAULT_VALUES, ERROR_CODE } = require('../helper/constants')
const { getPagination } = require('../helper/helper')
const { SuccessResponse, ErrorResponse } = require('../helper/response')
const Voucher = require('../model/voucher.model')
const { Op, where } = require('sequelize')

class VoucherController {
    create = async (req, res) => {
        try {
            const { name, description, code, discountPercent, quantity, startTime, expireTime } = req.body
            if (!name || !description || !code || !discountPercent || !quantity || !expireTime) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }
            const startDate = startTime || moment(new Date())
            const endDate = moment(startDate).add(expireTime, 'd')

            const token = req.token
            const verifyToken = jwt.verify(token, process.env.SECRET_KEY)

            const dataVoucher = {
                id: uuid.v4(),
                name,
                description,
                code,
                discountPercent,
                startDate,
                endDate,
                quantity,
                isDeleted: DEFAULT_VALUES.IS_NOT_DELETE,
                createdBy: verifyToken.id,
                updatedBy: verifyToken.id
            }

            const voucherCreated = await Voucher.create(dataVoucher)
            new SuccessResponse(res, HTTP_CODE.CREATED_SUCCESS, MESSAGE.CREATED_SUCCESS, voucherCreated)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }

    }

    getAll = async (req, res) => {
        try {
            let { page, size } = req.query
            if (!Number.isInteger(+page)) {
                page = DEFAULT_VALUES.DEFAULT_PAGE
            }
            if (!Number.isInteger(+size)) {
                size = DEFAULT_VALUES.DEFAULT_SIZE
            }

            const { limit, offset } = getPagination(page, size)

            const voucherCondition = {
                limit,
                offset,
                where: {
                    isDeleted: DEFAULT_VALUES.IS_NOT_DELETE
                }
            }
            const vouchers = await Voucher.findAndCountAll(voucherCondition)

            // RESPONSE
            const dataResponse = {
                pageIndex: parseInt(page) || DEFAULT_VALUES.PAGE_INDEX,
                currentPageSize: vouchers.rows.length,
                totalPage: Math.ceil(vouchers.count / size),
                totalSize: vouchers.count,
                data: vouchers.rows
            }
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, dataResponse)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    getNoExpire = async (req, res) => {
        try {
            let { page, size } = req.query
            if (!Number.isInteger(+page)) {
                page = DEFAULT_VALUES.DEFAULT_PAGE
            }
            if (!Number.isInteger(+size)) {
                size = DEFAULT_VALUES.DEFAULT_SIZE
            }

            const { limit, offset } = getPagination(page, size)

            const voucherCondition = {
                limit,
                offset,
                where: {
                    isDeleted: DEFAULT_VALUES.IS_NOT_DELETE,
                    endDate: {
                        [Op.gt]: moment()
                    }
                }
            }
            const vouchers = await Voucher.findAndCountAll(voucherCondition)

            // RESPONSE
            const dataResponse = {
                pageIndex: parseInt(page) || DEFAULT_VALUES.PAGE_INDEX,
                currentPageSize: vouchers.rows.length,
                totalPage: Math.ceil(vouchers.count / size),
                totalSize: vouchers.count,
                data: vouchers.rows
            }
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, dataResponse)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    getOne = async (req, res) => {
        const voucherId = req.params.id
        if (!voucherId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }
        const voucherCondition = { where: { id: voucherId } }

        try {
            const voucher = await Voucher.findOne(voucherCondition)
            if (!voucher) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, voucher)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    update = async (req, res) => {
        const voucherId = req.params.id
        if (!voucherId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }
        const oldVoucher = await Voucher.findOne({ where: { id: voucherId } })
        if (!oldVoucher) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        try {
            let data = req.body

            const startDate = data.startTime || oldVoucher.startDate
            const endDate = (data.expireTime) ? moment(startDate).add(data.expireTime, 'd') : oldVoucher.endDate

            const token = req.token
            const verifyToken = jwt.verify(token, process.env.SECRET_KEY)

            const dataUpdate = {
                ...data,
                startDate,
                endDate,
                isDeleted: DEFAULT_VALUES.IS_NOT_DELETE,
                createdBy: oldVoucher.createdBy,
                updatedBy: verifyToken.id
            }
            const updateCondition = { where: { id: voucherId } }

            await Voucher.update(dataUpdate, updateCondition)
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    delete = async (req, res) => {
        const voucherId = req.params.id
        if (!voucherId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        try {
            const dataDetele = {
                isDeleted: DEFAULT_VALUES.IS_DELETE
            }
            const deleteCondition = { where: { id: voucherId } }
            const checkVoucher = await Voucher.findOne(deleteCondition)
            if (!checkVoucher) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }
            
            await Voucher.update(dataDetele, deleteCondition)
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.DELETED_SUCCESS)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }
}

module.exports = new VoucherController