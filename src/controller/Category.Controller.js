const uuid = require('uuid')
const fs = require('fs')
const path = require('path')

const Category = require('../model/category.model')
const SubCategory = require('../model/subCategory.model')
const Product = require('../model/product.model')
const { HTTP_CODE, MESSAGE, DEFAULT_VALUES, ERROR_CODE } = require('../helper/constants')
const { SuccessResponse, ErrorResponse } = require('../helper/response')
const { getPagination } = require('../helper/helper')

class CategoryController {

    createSubCategory = async (req, res) => {
        try {
            const { categoryId, name } = req.body
            const image = req.file.filename

            const dataSubCategory = {
                id: uuid.v4(),
                categoryId,
                name,
                image,
                isDeleted: DEFAULT_VALUES.IS_NOT_DELETE
            }

            const subCategoryCreated = await SubCategory.create(dataSubCategory)
            if (!subCategoryCreated) {
                throw new Error
            }
            new SuccessResponse(res, HTTP_CODE.CREATED_SUCCESS, MESSAGE.CREATED_SUCCESS, subCategoryCreated)
        } catch (error) {
            fs.unlink(req.file.path, () => { })
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    createCategory = async (req, res) => {
        const { name } = req.body
        const image = req.file.filename

        const dataCategory = {
            id: uuid.v4(),
            name,
            image,
            isDeleted: DEFAULT_VALUES.IS_NOT_DELETE
        }
        try {
            const categoryCreated = await Category.create(dataCategory)
            if (!categoryCreated) {
                throw new Error
            }
            new SuccessResponse(res, HTTP_CODE.CREATED_SUCCESS, MESSAGE.CREATED_SUCCESS, categoryCreated)
        } catch (error) {
            fs.unlink(req.file.path, () => { })
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    updateCategory = async (req, res) => {
        const categoryId = req.params.id
        const data = req.body
        const image = req.file.filename
        if (!categoryId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        try {
            const checkCategory = await Category.findOne({ where: { id: categoryId } })
            if (!checkCategory) {
                const categoryPath = path.join(__dirname, `../../public/uploads/${image}`)
                fs.unlink(categoryPath, () => { })
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }
            const dataCategoryUpdate = {
                ...data,
                image,
                isDeleted: DEFAULT_VALUES.IS_NOT_DELETE
            }
            const categoryCondition = { where: { id: categoryId } }

            const category = await Category.findOne(categoryCondition)
            const categoryPath = path.join(__dirname, `../../public/uploads/${category.image}`)

            const categoryUpdated = await Category.update(dataCategoryUpdate, categoryCondition)
            if (categoryUpdated) {
                fs.unlink(categoryPath, () => { })
                new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS)
            } else {
                throw new Error
            }
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    updateSubCategory = async (req, res) => {
        const subCategoryId = req.params.id
        const data = req.body
        const image = req.file.filename
        if (!subCategoryId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        try {
            const checkSubCategory = await SubCategory.findOne({ where: { id: subCategoryId } })
            if (!checkSubCategory) {
                const subcategoryPath = path.join(__dirname, `../../public/uploads/${image}`)
                fs.unlink(subcategoryPath, () => { })
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            const dataSubCateUpdate = {
                ...data,
                image,
                isDeleted: DEFAULT_VALUES.IS_NOT_DELETE
            }
            const SubCateCondition = { where: { id: subCategoryId } }

            const subCategory = await SubCategory.findOne(SubCateCondition)
            const subCategoryPath = path.join(__dirname, `../../public/uploads/${subCategory.image}`)

            const subCateUpdated = await SubCategory.update(dataSubCateUpdate, SubCateCondition)
            if (subCateUpdated) {
                fs.unlink(subCategoryPath, () => { })
                new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS)
            } else {
                throw new Error
            }
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    deleteCategory = async (req, res) => {
        try {
            const categoryId = req.params.id
            if (!categoryId) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            const checkCategory = await Category.findOne({ where: { id: categoryId } })
            if (!checkCategory) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            const category = await Category.findAll({
                where: {
                    id: categoryId
                },
                include: {
                    model: SubCategory,
                    where: {
                        isDeleted: DEFAULT_VALUES.IS_NOT_DELETE
                    },
                    right: true
                }
            })

            if (category.length > 0) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.CANNOT_DELETE)
            }

            const dataDelete = {
                isDeleted: DEFAULT_VALUES.IS_DELETE
            }
            const cateCondition = { where: { id: categoryId } }

            const categoryDeleted = await Category.update(dataDelete, cateCondition)
            if (categoryDeleted) {
                new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.DELETED_SUCCESS)
            } else {
                throw new Error
            }

        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    deleteSubCategory = async (req, res) => {
        const subCategoryId = req.params.id
        if (!subCategoryId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        try {
            const checkSubcategory = await SubCategory.findOne({ where: { id: subCategoryId } })
            if (!checkSubcategory) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            const dataDelete = {
                isDeleted: DEFAULT_VALUES.IS_DELETE
            }
            const SubCateCondition = { where: { id: subCategoryId } }

            const products = await Product.findAll({ where: { subCategoryId } })
            if (products.length > 0) {
                return res.status(HTTP_CODE.BAD_REQUEST).json({ message: MESSAGE.CANNOT_DELETE })
            }

            const subCateDeleted = await SubCategory.update(dataDelete, SubCateCondition)
            if (subCateDeleted) {
                new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.DELETED_SUCCESS)
            } else {
                throw new Error
            }
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    getAll = async (req, res) => {
        try {
            let { page, size } = req.query
            if (!Number.isInteger(+page)) {
                page = DEFAULT_VALUES.DEFAULT_PAGE
            }
            if (!Number.isInteger(+size)) {
                size = DEFAULT_VALUES.DEFAULT_SIZE
            }

            const { limit, offset } = getPagination(page, size)

            const categoryCondition = {
                limit,
                offset,
                where: {
                    isDeleted: DEFAULT_VALUES.IS_NOT_DELETE
                },
                include: {
                    model: SubCategory,
                    where: {
                        isDeleted: DEFAULT_VALUES.IS_NOT_DELETE
                    }
                },
                distinct: true
            }

            const categories = await Category.findAndCountAll(categoryCondition)

            // RESPONSE
            const dataResponse = {
                pageIndex: parseInt(page) || DEFAULT_VALUES.PAGE_INDEX,
                currentPageSize: categories.rows.length,
                totalPage: Math.ceil(categories.count / size),
                totalSize: categories.count,
                data: categories.rows
            }
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, dataResponse)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    getOne = async (req, res) => {
        const categoryId = req.params.id
        if (!categoryId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        try {
            const checkCategory = await Category.findOne({ where: { id: categoryId } })
            if (!checkCategory) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            const categoryCondition = {
                where: {
                    id: categoryId,
                    isDeleted: DEFAULT_VALUES.IS_NOT_DELETE
                },
                include: [{ model: SubCategory }]
            }

            const category = await Category.findOne(categoryCondition)
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, category)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

}

module.exports = new CategoryController