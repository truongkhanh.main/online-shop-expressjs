const fs = require('fs')
const uuid = require('uuid')
const jwt = require('jsonwebtoken')
const path = require('path')
const { Op } = require('sequelize')

const { sequelize } = require('../config/database')
const Product = require('../model/product.model')
const ProductImage = require('../model/productImage.model')
const FlashSale = require('../model/flashSale.model')
const FlashSaleProduct = require('../model/flashSaleProduct.model')
const OrderDetail = require('../model/orderDetail.model')
const Order = require('../model/order.model')
const { HTTP_CODE, MESSAGE, DEFAULT_VALUES, STATUS_ORDER, ERROR_CODE } = require('../helper/constants')
const { SuccessResponse, ErrorResponse } = require('../helper/response')
const { totalPriceProduct, getPagination } = require('../helper/helper')

class ProductController {
    getAll = async (req, res) => {
        try {
            let { page, size } = req.query
            if (!Number.isInteger(+page)) {
                page = DEFAULT_VALUES.DEFAULT_PAGE
            }
            if (!Number.isInteger(+size)) {
                size = DEFAULT_VALUES.DEFAULT_SIZE
            }

            const { limit, offset } = getPagination(page, size)

            const productCondition = {
                limit,
                offset,
                where: { isDeleted: DEFAULT_VALUES.IS_NOT_DELETE },
                order: [['createdAt', DEFAULT_VALUES.DESC_SORT]]
            }

            const products = await Product.findAndCountAll(productCondition)

            // RESPONSE
            const dataResponse = {
                pageIndex: parseInt(page) || DEFAULT_VALUES.PAGE_INDEX,
                currentPageSize: products.rows.length,
                totalPage: Math.ceil(products.count / size),
                totalSize: products.count,
                data: products.rows
            }
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, dataResponse)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    getOne = async (req, res) => {
        try {
            const productId = req.params.id
            if (!productId) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }
            const productCondition = {
                where: {
                    id: productId,
                    isDeleted: DEFAULT_VALUES.IS_NOT_DELETE
                },
                include: [{ model: ProductImage }]
            }

            const product = await Product.findOne(productCondition)
            if (!product) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, product)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    getByCategory = async (req, res) => {
        const subcategoryId = req.params.subcategoryId
        if (!subcategoryId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        let { page, size } = req.query
        if (!Number.isInteger(+page)) {
            page = DEFAULT_VALUES.DEFAULT_PAGE
        }
        if (!Number.isInteger(+size)) {
            size = DEFAULT_VALUES.DEFAULT_SIZE
        }

        const { limit, offset } = getPagination(page, size)

        try {
            const productCondition = {
                limit,
                offset,
                where: {
                    subcategoryId
                },
                order: [['createdAt', DEFAULT_VALUES.DESC_SORT]]
            }

            const products = await Product.findAndCountAll(productCondition)

            // RESPONSE
            const dataResponse = {
                pageIndex: parseInt(page) || DEFAULT_VALUES.PAGE_INDEX,
                currentPageSize: products.rows.length,
                totalPage: Math.ceil(products.count / size),
                totalSize: products.count,
                data: products.rows
            }
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, dataResponse)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    create = async (req, res) => {
        const { name, description, barcode, importPrice, price, quantity, subcategoryId } = req.body
        const urls = req.files
        if (!name || !description || !barcode || !importPrice || !price || !quantity || !subcategoryId || (urls && urls.length === 0)) {
            for (const url of urls) {
                const productPath = path.join(__dirname, `../../public/uploads/${url.filename}`)
                fs.unlink(productPath, () => { })
            }
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }
        const tax = DEFAULT_VALUES.PRODUCT_TAX
        const transaction = await sequelize.transaction()
        try {
            const totalPrice = totalPriceProduct(price, tax)

            const token = req.token
            const verifyToken = jwt.verify(token, process.env.SECRET_KEY)

            const product = {
                id: uuid.v4(),
                name,
                description,
                barcode,
                importPrice,
                price,
                tax,
                totalPrice,
                allowedQuantity: quantity,
                actualQuantity: quantity,
                isDeleted: DEFAULT_VALUES.IS_NOT_DELETE,
                createdBy: verifyToken.id,
                subcategoryId
            }

            const productCreated = await Product.create(product, { transaction })

            const imagesProduct = urls.map(url => {
                return {
                    id: uuid.v4(),
                    name: name,
                    url: url.filename,
                    productId: productCreated.id,
                    isDeleted: DEFAULT_VALUES.IS_NOT_DELETE,
                    createdBy: verifyToken.id
                }
            })
            const prdImgCreated = await ProductImage.bulkCreate(imagesProduct, { transaction })
            await transaction.commit()

            // RESPONSE
            const dataResponse = {
                productCreated,
                prdImgCreated
            }
            new SuccessResponse(res, HTTP_CODE.CREATED_SUCCESS, MESSAGE.CREATED_SUCCESS, dataResponse)
        } catch (error) {
            await transaction.rollback()
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    update = async (req, res) => {
        const productId = req.params.id
        if (!productId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        const urls = req.files
        let data = req.body
        const tax = DEFAULT_VALUES.PRODUCT_TAX

        const transaction = await sequelize.transaction()
        try {
            const productCondition = { where: { id: productId }, transaction }
            const productImgCondition = { where: { productId }, transaction }

            const product = await Product.findOne(productCondition)
            if (!product) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }
            data.price = data.price || product.price
            data.name = data.name || product.name
            const totalPrice = totalPriceProduct(data.price, tax)
            const allowedQuantity = data.quantity || product.allowedQuantity
            const actualQuantity = data.quantity || product.actualQuantity

            const token = req.token
            const verifyToken = jwt.verify(token, process.env.SECRET_KEY)

            const dataUpdate = {
                ...data,
                allowedQuantity,
                actualQuantity,
                totalPrice,
                updatedBy: verifyToken.id
            }

            await Product.update(
                dataUpdate,
                productCondition
            )

            // Delete old product images and update new image
            if (urls.length > 0) {
                const productImages = await ProductImage.findAll(productImgCondition)
                const createdBy = productImages[0].createdBy
                for (const key in productImages) {
                    const productPath = path.join(__dirname, `../../public/uploads/${productImages[key].url}`)
                    fs.unlink(productPath, () => { })
                }
                await ProductImage.destroy(productImgCondition)

                const imagesProduct = urls.map(url => {
                    return {
                        id: uuid.v4(),
                        ...data,
                        url: url.filename,
                        productId,
                        isDeleted: DEFAULT_VALUES.IS_NOT_DELETE,
                        createdBy,
                        updatedBy: verifyToken.id
                    }
                })

                await ProductImage.bulkCreate(imagesProduct, { transaction })
            }

            await transaction.commit()
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS)
        } catch (error) {
            await transaction.rollback()
            for (const key in urls) {
                fs.unlink(urls[key].path, () => { })
            }
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    delete = async (req, res) => {
        const productId = req.params.id
        if (!productId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        const transaction = await sequelize.transaction()
        try {
            const dataUpdate = { isDeleted: DEFAULT_VALUES.IS_DELETE }

            // Check product in some order or not
            const orderCondition = {
                where: {
                    status: {
                        [Op.notIn]: [STATUS_ORDER.CANCELED, STATUS_ORDER.DELIVERIED]
                    }
                },
                include: {
                    model: OrderDetail,
                    where: {
                        productId
                    }
                }
            }

            const productInOrder = await Order.findAll(orderCondition)

            if (productInOrder.length > 0) {
                new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.NO_DELETE_PRODUCT)
            }

            const productCondition = { where: { id: productId }, transaction }
            const productImgCondition = { where: { productId }, transaction }

            const product = await Product.findOne(productCondition)
            if (!product) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
            }

            await Product.update(
                dataUpdate,
                productCondition
            )
            await ProductImage.update(
                dataUpdate,
                productImgCondition
            )

            await transaction.commit()
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.DELETED_SUCCESS)
        } catch (error) {
            await transaction.rollback()
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    deleteMany = async (req, res) => {
        const { listProductId } = req.body
        if (!listProductId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        try {
            const dataUpdate = { isDeleted: DEFAULT_VALUES.IS_DELETE }

            // Check product in some order or not
            const orderCondition = {
                where: {
                    status: {
                        [Op.notIn]: [STATUS_ORDER.CANCELED, STATUS_ORDER.DELIVERIED]
                    }
                },
                include: {
                    model: OrderDetail,
                    where: {
                        productId: listProductId
                    }
                }
            }

            const productInOrder = await Order.findAll(orderCondition)

            if (productInOrder.length > 0) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.NO_DELETE_PRODUCT)
            }
            const transaction = await sequelize.transaction()
            // Delete product and product image
            const productCondition = {
                where: {
                    id: { [Op.in]: listProductId }
                },
                transaction
            }
            await Product.update(dataUpdate, productCondition)

            for (const productId of listProductId) {
                const productImgCondition = { where: { productId }, transaction }
                await ProductImage.update(dataUpdate, productImgCondition)
            }

            await transaction.commit()
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.DELETED_SUCCESS)
        } catch (error) {
            await transaction.rollback()
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }

    }

    createProductSale = async (req, res) => {
        const { productId, flashsaleId } = req.body
        if (!productId || !flashsaleId) {
            return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.BAD_REQUEST)
        }

        try {
            const productSale = await FlashSaleProduct.findAll({
                where: {
                    productId
                }
            })
            if (productSale.length !== 0) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.PRODUCT_EXISTED)
            }

            const flashsale = await FlashSale.findOne({ where: { id: flashsaleId } })
            if (new Date(flashsale.endDate) < new Date()) {
                return new ErrorResponse(res, HTTP_CODE.BAD_REQUEST, ERROR_CODE.BAD_REQUEST, MESSAGE.SALE_EXPIRE)
            }

            const token = req.token
            const verifyToken = jwt.verify(token, process.env.SECRET_KEY)

            const dataProductSale = {
                id: uuid.v4(),
                discountPercent: flashsale.discountPercent,
                startDate: flashsale.startDate,
                endDate: flashsale.endDate,
                createdBy: verifyToken.id,
                productId,
                flashsaleId,
                isDeleted: DEFAULT_VALUES.IS_NOT_DELETE
            }

            const creatProductSale = await FlashSaleProduct.create(dataProductSale)
            if (creatProductSale) {
                new SuccessResponse(res, HTTP_CODE.CREATED_SUCCESS, MESSAGE.CREATED_SUCCESS, creatProductSale)
            } else {
                throw new Error
            }
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }

    }

    getProductSale = async (req, res) => {
        try {
            let { page, size } = req.query
            if (!Number.isInteger(+page)) {
                page = DEFAULT_VALUES.DEFAULT_PAGE
            }
            if (!Number.isInteger(+size)) {
                size = DEFAULT_VALUES.DEFAULT_SIZE
            }

            const { limit, offset } = getPagination(page, size)

            const productCondition = {
                limit,
                offset,
                where: {
                    isDeleted: DEFAULT_VALUES.IS_NOT_DELETE
                },
                include: {
                    model: FlashSaleProduct,
                    right: true
                },
                distinct: true
            }

            const productSale = await Product.findAndCountAll(productCondition)
            if (!productSale) {
                throw new Error
            }

            // RESPONSE
            const dataResponse = {
                pageIndex: parseInt(page) || DEFAULT_VALUES.PAGE_INDEX,
                currentPageSize: productSale.rows.length,
                totalPage: Math.ceil(productSale.count / size),
                totalSize: productSale.count,
                data: productSale.rows
            }
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, dataResponse)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }

    searchByName = async (req, res) => {
        try {
            let { page, size } = req.query
            if (!Number.isInteger(+page)) {
                page = DEFAULT_VALUES.DEFAULT_PAGE
            }
            if (!Number.isInteger(+size)) {
                size = DEFAULT_VALUES.DEFAULT_SIZE
            }

            const { limit, offset } = getPagination(page, size)

            const { name } = req.query
            const dataSearch = {
                limit,
                offset,
                where: {
                    name: { [Op.substring]: name }
                },
                include: [{ model: ProductImage }],
                order: [['createdAt', DEFAULT_VALUES.DESC_SORT]]
            }

            const result = await Product.findAndCountAll(dataSearch)

            // RESPONSE
            const dataResponse = {
                pageIndex: parseInt(page) || DEFAULT_VALUES.PAGE_INDEX,
                currentPageSize: result.rows.length,
                totalPage: Math.ceil(result.count / size),
                totalSize: result.count,
                data: result.rows
            }
            new SuccessResponse(res, HTTP_CODE.RESPONSE_SUCCESS, MESSAGE.RESPONSE_SUCCESS, dataResponse)
        } catch (error) {
            new ErrorResponse(res, HTTP_CODE.SERVER_ERROR, ERROR_CODE.SERVER_ERROR, error.message)
        }
    }
}

module.exports = new ProductController