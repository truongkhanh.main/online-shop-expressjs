const express = require('express')
const db = require('./config/database')
const bodyParser = require('body-parser')
const dotenv = require('dotenv')

const route = require('./routes/index')
const swagger = require('../swagger/index')
// const task = require('./helper/cronjob')

dotenv.config()
db.connect()

const app = express()
const port = 3000

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

route(app)
swagger(app)
// task.start()

module.exports = app.listen(port, () => {
    console.log(`Example app listening at localhost:${port}`)
})