const express = require('express')
const router = express.Router()

const productController = require('../controller/product.controller')
const { verifyToken, permission } = require('../middleware/auth.middleware')
const { PERMISSION, API } = require('../helper/constants')
const upload = require('../helper/multer')

router.post('/', verifyToken, permission([PERMISSION.ADD]), upload.any(), productController.create)
router.post('/sale', verifyToken, permission([PERMISSION.ADD]), productController.createProductSale)
router.get('/sale', verifyToken, productController.getProductSale)
router.patch('/:id', verifyToken, permission([PERMISSION.UPDATE], API.PRODUCT), upload.any(), productController.update)
router.delete('/:id', verifyToken, permission([PERMISSION.DELETE], API.PRODUCT), productController.delete)
router.delete('/', verifyToken, permission([PERMISSION.DELETE]), productController.deleteMany)
router.get('/search', productController.searchByName)
router.get('/:id', productController.getOne)
router.get('/subcate/:subcategoryId', productController.getByCategory)
router.get('/', productController.getAll)

module.exports = router