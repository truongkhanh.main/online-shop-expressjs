const authRouter = require('./auth.route')
const userRouter = require('./user.route')
const voucherRouter = require('./voucher.route')
const productRouter = require('./product.route')
const categoryRouter = require('./category.route')
const flashsaleRouter = require('./flashSale.route')
const orderRouter = require('./order.route')

function route(app) {
    app.use('/api/v1/auth', authRouter)
    app.use('/api/v1/users', userRouter)
    app.use('/api/v1/vouchers', voucherRouter)
    app.use('/api/v1/products', productRouter)
    app.use('/api/v1/categories', categoryRouter)
    app.use('/api/v1/flash-sales', flashsaleRouter)
    app.use('/api/v1/orders', orderRouter)
}

module.exports = route