const express = require('express')
const router = express.Router()

const orderController = require('../controller/order.controller')
const { verifyToken, permission } = require('../middleware/auth.middleware')
const { PERMISSION, API } = require('../helper/constants')

router.post('/', verifyToken, orderController.create)
router.patch('/:id/confirm', verifyToken, permission([PERMISSION.UPDATE], API.ORDER_CONFIRM), orderController.updateConfirmStatus)
router.patch('/:id/shipping', verifyToken, permission([PERMISSION.UPDATE], API.ORDER_SHIPPING), orderController.updateShippingStatus)
router.patch('/:id/delivered', verifyToken, permission([PERMISSION.UPDATE], API.ORDER_DELIVERED), orderController.updateDeliveredStatus)
router.patch('/:id/cancel', verifyToken, permission([PERMISSION.UPDATE], API.ORDER_CANCEL), orderController.cancelOrder)
router.delete('/:id', verifyToken, permission([PERMISSION.DELETE], API.ORDER), orderController.delete)
router.get('/:id', verifyToken, orderController.getOne)
router.get('/', verifyToken, orderController.getAll)
// router.post('/test', verifyToken, orderController.testCreateOrder)

module.exports = router