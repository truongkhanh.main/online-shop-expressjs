const express = require('express')
const router = express.Router()

const flashsaleController = require('../controller/flashSale.controller')
const { verifyToken, permission } = require('../middleware/auth.middleware')
const { PERMISSION, API } = require('../helper/constants')

router.post('/', verifyToken, permission([PERMISSION.ADD]), flashsaleController.create)
router.get('/no-expire', flashsaleController.getNoExpire)
router.patch('/:id', verifyToken, permission([PERMISSION.UPDATE], API.FLASH_SALE), flashsaleController.update)
router.delete('/:id', verifyToken, permission([PERMISSION.DELETE], API.FLASH_SALE), flashsaleController.delete)
router.get('/:id', verifyToken, permission([PERMISSION.READ], API.FLASH_SALE), flashsaleController.getOne)
router.get('/', verifyToken, permission([PERMISSION.READ]), flashsaleController.getAll)

module.exports = router