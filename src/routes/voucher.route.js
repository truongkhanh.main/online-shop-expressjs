const express = require('express')
const router = express.Router()

const voucherController = require('../controller/voucher.controller')
const { verifyToken, permission } = require('../middleware/auth.middleware')
const { PERMISSION, API } = require('../helper/constants')

router.post('/', verifyToken, permission([PERMISSION.ADD]), voucherController.create)
router.get('/no-expire', voucherController.getNoExpire)
router.patch('/:id', verifyToken, permission([PERMISSION.UPDATE], API.VOUCHER), voucherController.update)
router.delete('/:id', verifyToken, permission([PERMISSION.DELETE], API.VOUCHER), voucherController.delete)
router.get('/:id', voucherController.getOne)
router.get('/', verifyToken, permission([PERMISSION.READ]), voucherController.getAll)

module.exports = router