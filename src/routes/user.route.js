const express = require('express')
const router = express.Router()

const userController = require('../controller/user.controller')
const { verifyToken, permission } = require('../middleware/auth.middleware')
const { PERMISSION, API } = require('../helper/constants')

router.patch('/:id', verifyToken, userController.updateInfo)
router.delete('/:id', verifyToken, permission([PERMISSION.DELETE], API.USER), userController.inactive)
router.get('/:id', verifyToken, userController.getOne)
router.get('/', verifyToken, permission([PERMISSION.READ], API.USERS), userController.listUser)

module.exports = router