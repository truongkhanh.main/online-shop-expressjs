const express = require('express')
const router = express.Router()

const categoryController = require('../controller/category.controller')
const { verifyToken, permission } = require('../middleware/auth.middleware')
const { PERMISSION, API } = require('../helper/constants')
const upload = require('../helper/multer')

router.post('/subcate', verifyToken, permission([PERMISSION.ADD]), upload.single('image'), categoryController.createSubCategory)
router.patch('/subcate/:id', verifyToken, permission([PERMISSION.UPDATE], API.SUBCATE), upload.single('image'), categoryController.updateSubCategory)
router.delete('/subcate/:id', verifyToken, permission([PERMISSION.DELETE], API.SUBCATE), categoryController.deleteSubCategory)

router.post('/cate', verifyToken, permission([PERMISSION.ADD]), upload.single('image'), categoryController.createCategory)
router.patch('/cate/:id', verifyToken, permission([PERMISSION.UPDATE], API.CATE), upload.single('image'), categoryController.updateCategory)
router.delete('/cate/:id', verifyToken, permission([PERMISSION.DELETE], API.CATE), categoryController.deleteCategory)

router.get('/', categoryController.getAll)
router.get('/:id', categoryController.getOne)

module.exports = router
