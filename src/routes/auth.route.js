const express = require('express')
const router = express.Router()

const { verifyToken } = require('../middleware/auth.middleware')
const authController = require('../controller/auth.controller')

router.post('/register', authController.register)
router.get('/verify/:username/:verifyCode', authController.verify)
router.post('/login', authController.login)
router.post('/resend-email', authController.resendEmail)
router.post('/forgotpw', authController.forgotPassword)
router.patch('/changepw', verifyToken, authController.changePassword)
router.post('/rolemodule', authController.rolemodule)

module.exports = router