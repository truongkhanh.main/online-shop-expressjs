const { Sequelize } = require('sequelize')
const config = require('./index')

const option = {
    username: config.db_username,
    password: config.db_password,
    database: config.db_database,
    host: config.db_host,
    dialect: config.db_dialect,
    dialectOptions: {
        useUTC: false,
        dateStrings: true,
        typeCast: true
    },
    collate: config.db_collate,
    timezone: config.db_timezone,

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
}
const sequelize = new Sequelize(option)
const connect = () => {
    sequelize.authenticate()
        .then(() => console.log('Database connected...'))
        .catch(err => console.log('Error: ', err))
}

// sequelize.sync({force: true})
// sequelize.sync({alter: true})
// sequelize.sync()

module.exports = { sequelize, connect }