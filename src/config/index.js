require('dotenv').config()

const configs = {
    base: {
        // Database
        db_host: process.env.DATABASE_HOST,
        db_dialect: process.env.DATABASE_DIALECT,
        db_username: process.env.DATABASE_USERNAME,
        db_password: process.env.DATABASE_PASSWORD,
        db_database: process.env.DATABASE_NAME,
        db_collate: process.env.DATABASE_COLLATE,
        db_timezone: process.env.DATABASE_TIMEZONE,
    }
}

const config = Object.assign(configs.base)

module.exports = config