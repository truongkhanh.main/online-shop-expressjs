const { DataTypes } = require('sequelize')
const { sequelize } = require('../config/database')

const SubCategory = require('./subCategory.model')

const Product = sequelize.define('products', {
    id: {
        type: DataTypes.STRING(36),
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false
    },
    barcode: {
        type: DataTypes.STRING(45),
        allowNull: false
    },
    importPrice: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    price: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    tax: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    totalPrice: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    allowedQuantity: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    actualQuantity: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    isDeleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    createdBy: {
        type: DataTypes.STRING,
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING,
        allowNull: true
    }
}, { timestamps: true })

SubCategory.hasMany(Product, {
    foreignKey: {
        name: 'subcategoryId',
        allowNull: false
    }
})
Product.belongsTo(SubCategory, {
    foreignKey: {
        name: 'subcategoryId',
        allowNull: false
    }
})

module.exports = Product
