const { DataTypes } = require('sequelize')
const { sequelize } = require('../config/database')
const User = require('./user.model')

const Customer = sequelize.define('customers', {
    id: {
        type: DataTypes.STRING(36),
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    age: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    phone: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    address: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    isActive: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    createdBy: {
        type: DataTypes.STRING,
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING,
        allowNull: true
    }
}, { timestamps: true })

User.hasOne(Customer, {
    foreignKey: {
        name: 'userId',
        allowNull: false
    }
})
Customer.belongsTo(User, {
    foreignKey: {
        name: 'userId',
        allowNull: false
    }
})

module.exports = Customer