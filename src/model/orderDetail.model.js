const { DataTypes } = require('sequelize')
const { sequelize } = require('../config/database')
const Product = require('./product.model')
const Order = require('./order.model')

const OrderDetail = sequelize.define('orderdetail', {
    id: {
        type: DataTypes.STRING(36),
        primaryKey: true
    },
    quantity: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    price: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    isDeleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    createdBy: {
        type: DataTypes.STRING,
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING,
        allowNull: true
    }
}, { timestamps: true })

Order.hasMany(OrderDetail, {
    foreignKey: {
        name: 'orderId',
        allowNull: false
    }
})
OrderDetail.belongsTo(Order, {
    foreignKey: {
        name: 'orderId',
        allowNull: false
    }
})

Product.hasMany(OrderDetail, {
    foreignKey: {
        name: 'productId',
        allowNull: false
    }
})
OrderDetail.belongsTo(Product, {
    foreignKey: {
        name: 'productId',
        allowNull: false
    }
})

module.exports = OrderDetail