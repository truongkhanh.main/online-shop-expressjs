const { DataTypes } = require('sequelize')
const { sequelize } = require('../config/database')
const Role = require('./role.model')

const RoleModules = sequelize.define('rolemodules', {
    id: {
        type: DataTypes.STRING(36),
        primaryKey: true
    },
    api: {
        type: DataTypes.STRING,
        allowNull: false
    },
    isCanRead: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    isCanAdd: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    isCanEdit: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    isCanDelete: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    createdBy: {
        type: DataTypes.STRING,
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING,
        allowNull: true
    }
}, { timestamps: true })

Role.hasMany(RoleModules, {
    foreignKey: {
        name: 'roleId',
        allowNull: false
    }
})
RoleModules.belongsTo(Role, {
    foreignKey: {
        name: 'roleId',
        allowNull: false
    }
})

module.exports = RoleModules
