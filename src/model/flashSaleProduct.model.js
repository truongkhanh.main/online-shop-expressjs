const { DataTypes } = require('sequelize')
const { sequelize } = require('../config/database')

const Product = require('./product.model')
const FlashSale = require('./flashSale.model')

const FlashSaleProduct = sequelize.define('flashsaleproduct', {
    id: {
        type: DataTypes.STRING(36),
        primaryKey: true
    },
    discountPercent: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    startDate: {
        type: DataTypes.DATE,
        allowNull: false
    },
    endDate: {
        type: DataTypes.DATE,
        allowNull: false
    },
    isDeleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    createdBy: {
        type: DataTypes.STRING,
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING,
        allowNull: true
    }
}, { timestamps: true })

Product.hasMany(FlashSaleProduct, {
    foreignKey: {
        name: 'productId',
        allowNull: false
    }
})
FlashSaleProduct.belongsTo(Product, {
    foreignKey: {
        name: 'productId',
        allowNull: false
    }
})

FlashSale.hasMany(FlashSaleProduct, {
    foreignKey: {
        name: 'flashsaleId',
        allowNull: false
    }
})
FlashSaleProduct.belongsTo(FlashSale, {
    foreignKey: {
        name: 'flashsaleId',
        allowNull: false
    }
})

module.exports = FlashSaleProduct

