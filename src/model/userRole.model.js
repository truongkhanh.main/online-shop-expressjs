const { DataTypes } = require('sequelize')
const { sequelize } = require('../config/database')

const User = require('./user.model')
const Role = require('./role.model')

const UserRole = sequelize.define('userrole', {
    id: {
        type: DataTypes.STRING(36),
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false
    },
    createdBy: {
        type: DataTypes.STRING,
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING,
        allowNull: true
    }
}, { timestamps: true })

User.hasMany(UserRole, {
    foreignKey: {
        name: 'userId',
        allowNull: false
    }
})
UserRole.belongsTo(User, {
    foreignKey: {
        name: 'userId',
        allowNull: false
    }
})

Role.hasMany(UserRole, {
    foreignKey: {
        name: 'roleId',
        allowNull: false
    }
})
UserRole.belongsTo(Role, {
    foreignKey: {
        name: 'roleId',
        allowNull: false
    }
})

module.exports = UserRole
