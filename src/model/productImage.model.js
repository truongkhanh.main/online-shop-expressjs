const { DataTypes } = require('sequelize')
const { sequelize } = require('../config/database')
const Product = require('./product.model')

const ProductImage = sequelize.define('productimage', {
    id: {
        type: DataTypes.STRING(36),
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    url: {
        type: DataTypes.STRING,
        allowNull: false
    },
    isDeleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    createdBy: {
        type: DataTypes.STRING,
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING,
        allowNull: true
    }
}, { timestamps: true })

Product.hasMany(ProductImage, {
    foreignKey: {
        name: 'productId',
        allowNull: false
    }
})
ProductImage.belongsTo(Product, {
    foreignKey: {
        name: 'productId',
        allowNull: false
    }
})

module.exports = ProductImage