const { DataTypes } = require('sequelize')
const { sequelize } = require('../config/database')

const Category = require('./category.model')

const SubCategory = sequelize.define('subcategory', {
    id: {
        type: DataTypes.STRING(36),
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    image: {
        type: DataTypes.STRING,
        allowNull: false
    },
    isDeleted: {
        type: DataTypes.BOOLEAN,
        allowNull: true
    },
    createdBy: {
        type: DataTypes.STRING,
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING,
        allowNull: true
    }
}, { timestamps: true })

Category.hasMany(SubCategory, {
    foreignKey: {
        name: 'categoryId',
        allowNull: false
    }
})
SubCategory.belongsTo(Category, {
    foreignKey: {
        name: 'categoryId',
        allowNull: false
    }
})

module.exports = SubCategory