const { DataTypes } = require('sequelize')
const { sequelize } = require('../config/database')

const User = sequelize.define('users', {
    id: {
        type: DataTypes.STRING(36),
        primaryKey: true
    },
    username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    verifyCode: {
        type: DataTypes.STRING,
        allowNull: false
    },
    verifyStatus: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    verifyTime: {
        type: DataTypes.DATE,
        allowNull: false
    },
    isActive: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    createdBy: {
        type: DataTypes.STRING,
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING,
        allowNull: true
    }
}, { timestamps: true })

module.exports = User