const { DataTypes } = require('sequelize')
const { sequelize } = require('../config/database')
const Customer = require('./customer.model')
const Voucher = require('./voucher.model')

const Order = sequelize.define('orders', {
    id: {
        type: DataTypes.STRING(36),
        primaryKey: true
    },
    orderCode: {
        type: DataTypes.STRING,
        allowNull: false
    },
    status: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    totalPrice: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    isDeleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    createdBy: {
        type: DataTypes.STRING,
        allowNull: true
    },
    updatedBy: {
        type: DataTypes.STRING,
        allowNull: true
    }
}, { timestamps: true })

Customer.hasMany(Order, {
    foreignKey: {
        name: 'customerId',
        allowNull: false
    }
})
Order.belongsTo(Customer, {
    foreignKey: {
        name: 'customerId',
        allowNull: false
    }
})

Voucher.hasMany(Order, {
    foreignKey: {
        name: 'voucherId',
        allowNull: true
    }
})
Order.belongsTo(Voucher, {
    foreignKey: {
        name: 'voucherId',
        allowNull: false
    }
})

module.exports = Order
